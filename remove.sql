DROP TRIGGER product_audit_trigger;
DROP TRIGGER store_audit_trigger;
DROP TRIGGER warehouse_audit_trigger;
DROP TRIGGER store_product_audit_trigger;
DROP TRIGGER warehouse_product_audit_trigger;
DROP TRIGGER review_audit_trigger;

DROP TABLE employees CASCADE CONSTRAINTS;
DROP TABLE customers CASCADE CONSTRAINTS;
DROP TABLE warehouses CASCADE CONSTRAINTS;
DROP TABLE stores CASCADE CONSTRAINTS;
DROP TABLE products CASCADE CONSTRAINTS;
DROP TABLE reviews CASCADE CONSTRAINTS;
DROP TABLE orders CASCADE CONSTRAINTS;
DROP TABLE store_products CASCADE CONSTRAINTS;
DROP TABLE warehouse_products CASCADE CONSTRAINTS;
DROP TABLE authentication_salt CASCADE CONSTRAINTS;
DROP TABLE customer_authentication CASCADE CONSTRAINTS;
DROP TABLE employee_authentication CASCADE CONSTRAINTS;
DROP TABLE audit_log CASCADE CONSTRAINTS;
DROP TABLE audit_log_auth CASCADE CONSTRAINTS;

DROP TYPE product_type;
DROP TYPE order_product_type;
DROP TYPE order_type;
DROP TYPE review_type;
DROP TYPE user_type;
DROP TYPE warehouse_type;
DROP TYPE warehouse_product_type;
DROP TYPE store_type;
DROP TYPE store_product_type;

DROP PACKAGE exceptions;
DROP PACKAGE util;
DROP PACKAGE store_customer_tools;
DROP PACKAGE store_manager_tools;
DROP PACKAGE authentication;