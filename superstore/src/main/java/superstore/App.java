package superstore;

import java.sql.SQLException;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;

import superstore.cmds.shared.*;
import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.dbutil.DatabaseConnection;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

public class App 
{
    public static final String CONNECTION_URL = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";

    public static void main( String[] args )
    {
        // Clear the console on startup.
        ConsoleHelper.clear();

        // Initialize object for the database connection.
        // Username and password are entered and validated through this constructor.
        // All exception handling and pretty-printing is handled inside of this constructor.
        DatabaseConnection c = new DatabaseConnection(CONNECTION_URL);

        // Initialize an object that will represent the logged in user.
        SuperstoreLogin s;

        // Prompt the user for their login type (customer or employee)
        String loginType = "";
        while (!loginType.equals("c") && !loginType.equals("e")) {
            System.out.println("Are you logging in as a customer " + ConsoleHelper.green("(c)") + " or employee " + ConsoleHelper.yellow("(e)") + "?");
            System.out.println("");
            loginType = System.console().readLine("Login type: ");
        }

        // When gathered, remind them what type of user they are logging in as.
        ConsoleHelper.clear();
        System.out.println("You are logging in as " + (loginType.equals("e") ? ConsoleHelper.yellow("an employee") : ConsoleHelper.green("a customer")));
        System.out.println("");
        
        while (true) {
            // Prompt for the user's email and password.
            String username = System.console().readLine("Email: ");
            String password = String.valueOf(System.console().readPassword("Password: "));

            // Create a new SuperstoreLogin with the database connection, login type, and email they just entered.
            s = new SuperstoreLogin(c.getConnection(), loginType, username);

            try {
                // Attempt to login with the given credentials.
                s.attemptLogin(password);
                break;
            }
            catch (SQLException e) {
                // If the login failed, tell the user what happened and prompt them again.
                ConsoleHelper.clear();
                System.out.println("Cannot login: " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)) + ". Please try again.");
                System.out.println("");
            }
        }
        
        if (s.isEmployee()) {
            // Hnadle employee commands
            while (true) {
                ConsoleHelper.clear();
                System.out.println("Hello, " + s.getFullName() + ". You are " + ConsoleHelper.yellow("an employee"));
                System.out.println("");
                
                // Show all base employee commands
                CommandRunner.promptCommandFromList(s, new ICommand[] {
                    new CommandViewerHeading("View"),
                    new ViewProducts(),
                    new ViewStores(),
                    new ViewWarehouses(),
                    new CommandViewerHeading("Search"),
                    new SearchProductsAcrossStores(),
                    new SearchProductsAcrossWarehouses(),
                    new CommandViewerHeading("Manage"),
                    new AddProduct(),
                    new AddStore(),
                    new AddWarehouse(),
                    new CommandViewerHeading("Moderation"),
                    new ViewFlaggedReviews(),
                    new CommandViewerHeading("Audit Logs"),
                    new ViewAuditLog(),
                    new ViewAuditLogAuth(),
                    new CommandViewerHeading("Statistics"),
                    new GetBestsellingProducts()
                });
            }
        }
        else {
            // Handle customer commands
            while (true) {
                ConsoleHelper.clear();
                System.out.println("Hello, " + s.getFullName() + ". You are " + ConsoleHelper.green("a customer"));
                
                // Show all base customer commands
                CommandRunner.promptCommandFromList(s, new ICommand[] {
                    new CommandViewerHeading("View"),
                    new ViewProducts(),
                    new ViewStores(),
                    new CommandViewerHeading("Search"),
                    new SearchProductsByCategory(),
                    new SearchProductsAcrossStores(),
                    new CommandViewerHeading("My Account"),
                    new ViewOrders(),
                    new ViewMyReviews()
                });
            }
        }
    }
}
