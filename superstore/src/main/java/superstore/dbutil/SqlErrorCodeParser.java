package superstore.dbutil;

import java.sql.SQLException;

public class SqlErrorCodeParser {
    
    /**
     * Attempts to convert the ORA-XXXXX error code from an SQLException into a human-readable error.
     * @param e The SQLException
     * @return A human-readable summary of the SQLException
     */
    public static String getHumanReadableError(SQLException e) {
        switch (e.getMessage().substring(0, 9)) {
            case "ORA-01005":
                return "Please enter a password";
            case "ORA-01017":
                return "Invalid username or password for database";
            case "ORA-01438":
                return "Value larger than expected was entered.";

            case "ORA-20001":
                return "This product does not exist";
            case "ORA-20002":
                return "This store does not exist";
            case "ORA-20003":
                return "This warehouse does not exist";
            case "ORA-20004":
                return "This review does not exist";

            case "ORA-20101":
                return "This product does not exist in this store";
            case "ORA-20102":
                return "This product does not exist in this warehouse";

            case "ORA-20111":
                return "This product already exists in this store";
            case "ORA-20112":
                return "This product already exists in this warehouse";

            case "ORA-20201":
                return "Not enough stock";

            case "ORA-20301":
                return "Review out of bounds (make sure your review is between 1 to 5)";

            case "ORA-20501":
                return "Invalid username or password for customer";
            case "ORA-20502":
                return "Invalid username or password for employee";

            default:
                return e.getMessage();
        }
    }
}
