package superstore.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import superstore.util.ConsoleHelper;

public class DatabaseConnection {
    private Connection connection;

    /**
     * Constructor that takes in a URL and prompts for the database credentials until the login is successful.
     * @param url The database connection URL
     */
    public DatabaseConnection(String url) {
        while (this.connection == null) {
            String username = System.console().readLine("Database username: ");
            String password = String.valueOf(System.console().readPassword("Database password: "));

            try {
                Connection conn = DriverManager.getConnection(url, username, password);
                this.connection = conn;
                this.configureCurrentConnection();

                ConsoleHelper.clear();
                System.out.println("Database connection established!");
            }
            catch (SQLException e) {
                ConsoleHelper.clear();
                System.out.println("Cannot establish connection; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)) + ". Please try again.");
                System.out.println("");
            }
        }
    }

    /**
     * Constructor that takes in a URL, username, and password. It will create a connection with that information.
     * @param url The database connection URL
     * @param username The username for the database connection
     * @param password The password for the database connection
     * @throws SQLException If the connection cannot be established
     */
    public DatabaseConnection(String url, String username, String password) throws SQLException {
        this.connection = DriverManager.getConnection(url, username, password);
        this.configureCurrentConnection();
    }

    /**
     * Sets auto-commit to false for the database connection and creates all of the necessary typemaps.
     */
    public void configureCurrentConnection() {
       try {
            if (this.connection == null || this.connection.isClosed()) return;

            this.connection.setAutoCommit(false);
            
            Map<String, Class<?>> map = this.connection.getTypeMap();
            this.connection.setTypeMap(map);
            map.put("USER_TYPE", Class.forName("superstore.dbtypes.SuperstoreUser"));
            map.put("PRODUCT_TYPE", Class.forName("superstore.dbtypes.Product"));
            map.put("STORE_TYPE", Class.forName("superstore.dbtypes.Store"));
            map.put("STORE_PRODUCT_TYPE", Class.forName("superstore.dbtypes.StoreProduct"));
            map.put("WAREHOUSE_PRODUCT_TYPE", Class.forName("superstore.dbtypes.WarehouseProduct"));
            map.put("ORDER_TYPE", Class.forName("superstore.dbtypes.Order"));
            map.put("REVIEW_TYPE", Class.forName("superstore.dbtypes.Review"));
       }
       catch (Exception e) {}
    }

    /**
     * Returns the current database connection
     * @return The database connection
     */
    public Connection getConnection() {
        return this.connection;
    }
}
