package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a store.
 * This object can be mapped to an SQL object.
 */
public class Store implements SQLData {
    private int id;
    private String name;

    public Store() { }

    public Store(String name) {
        this.name = name;
    }

    public Store(int id, String name) {
        this.id   = id;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.id = stream.readInt();
        this.name = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.id);
        stream.writeString(this.name);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "STORE_TYPE";
    }  
}
