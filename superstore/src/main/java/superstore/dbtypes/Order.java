package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a customer's order.
 * This object can be mapped to an SQL object.
 */
public class Order implements SQLData {
    private int customerId;
    private int storeId;
    private int productId;
    private int quantity;

    public Order() { }

    public Order(int customerId, int storeId, int productId, int quantity) {
        this.customerId = customerId;
        this.storeId    = storeId;
        this.productId  = productId;
        this.quantity   = quantity;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.customerId = stream.readInt();
        this.storeId    = stream.readInt();
        this.productId  = stream.readInt();
        this.quantity   = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.customerId);
        stream.writeInt(this.storeId);
        stream.writeInt(this.productId);
        stream.writeInt(this.quantity);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "ORDER_TYPE";
    }  
}
