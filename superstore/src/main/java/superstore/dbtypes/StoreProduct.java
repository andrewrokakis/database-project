package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a product in a store.
 * This object can be mapped to an SQL object.
 */
public class StoreProduct implements SQLData {
    private int storeId;
    private int productId;
    private int quantity;
    private double price;

    public StoreProduct() { }

    public StoreProduct(int storeId, int productId, int quantity, double price) {
        this.storeId   = storeId;
        this.productId = productId;
        this.quantity  = quantity;
        this.price     = price;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.storeId   = stream.readInt();
        this.productId = stream.readInt();
        this.quantity  = stream.readInt();
        this.price     = stream.readDouble();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.storeId);
        stream.writeInt(this.productId);
        stream.writeInt(this.quantity);
        stream.writeDouble(this.price);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "STORE_PRODUCT_TYPE";
    }  
}
