package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a customer's order.
 * This object can be mapped to an SQL object.
 */
public class Review implements SQLData {
    private int customerId;
    private int productId;
    private int review;
    private String description;

    public Review() { }

    public Review(int customerId, int productId, int review, String description) {
        this.customerId  = customerId;
        this.productId   = productId;
        this.review      = review;
        this.description = description;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.customerId  = stream.readInt();
        this.productId   = stream.readInt();
        this.review      = stream.readInt();
        this.description = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.customerId);
        stream.writeInt(this.productId);
        stream.writeInt(this.review);
        stream.writeString(this.description);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "REVIEW_TYPE";
    }  
}
