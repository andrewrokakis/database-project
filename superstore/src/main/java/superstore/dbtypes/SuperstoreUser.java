package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a user (either a customer or an employee).
 * This object can be mapped to an SQL object.
 */
public class SuperstoreUser implements SQLData {
    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String email;

    public SuperstoreUser() { }

    public SuperstoreUser(int id, String firstName, String lastName, String address, String email) {
        this.id        = id;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.address   = address;
        this.email     = email;
    }

    public int getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }
    
    public String getAddress() {
        return this.address;
    }

    public String getEmail() {
        return this.email;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.id = stream.readInt();
        this.firstName = stream.readString();
        this.lastName = stream.readString();
        this.address = stream.readString();
        this.email = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.id);
        stream.writeString(this.firstName);
        stream.writeString(this.lastName);
        stream.writeString(this.address);
        stream.writeString(this.email);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "USER_TYPE";
    }  
}
