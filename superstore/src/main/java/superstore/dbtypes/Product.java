package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a product.
 * This object can be mapped to an SQL object.
 */
public class Product implements SQLData {
    private int id;
    private String name;
    private String category;

    public Product() { }

    public Product(String name, String category) {
        this.name     = name;
        this.category = category;
    }

    public Product(int id, String name, String category) {
        this.id       = id;
        this.name     = name;
        this.category = category;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getCategory() {
        return this.category;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.id = stream.readInt();
        this.name = stream.readString();
        this.category = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.id);
        stream.writeString(this.name);
        stream.writeString(this.category);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "PRODUCT_TYPE";
    }  
}
