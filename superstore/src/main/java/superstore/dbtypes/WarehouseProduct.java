package superstore.dbtypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Object representing a product in a warehouse.
 * This object can be mapped to an SQL object.
 */
public class WarehouseProduct implements SQLData {
    private int warehouseId;
    private int productId;
    private int quantity;

    public WarehouseProduct() { }

    public WarehouseProduct(int warehouseId, int productId, int quantity) {
        this.warehouseId = warehouseId;
        this.productId   = productId;
        this.quantity    = quantity;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.warehouseId = stream.readInt();
        this.productId   = stream.readInt();
        this.quantity    = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.warehouseId);
        stream.writeInt(this.productId);
        stream.writeInt(this.quantity);
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "WAREHOUSE_PRODUCT_TYPE";
    }  
}
