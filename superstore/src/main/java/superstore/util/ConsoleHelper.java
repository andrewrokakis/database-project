package superstore.util;

public class ConsoleHelper {
    /**
     * Clears the console.
     */
    public static void clear() {
        System.out.print(ConsoleEscape.CLEAR);
    }

    /**
     * Makes a string red.
     * @param in The string to make red.
     * @return The red string.
     */
    public static String red(String in) {
        return ConsoleEscape.FG_RED + in + ConsoleEscape.RESET;
    }

    /**
     * Makes a string yellow.
     * @param in The string to make yellow.
     * @return The yellow string.
     */
    public static String yellow(String in) {
        return ConsoleEscape.FG_YELLOW + in + ConsoleEscape.RESET;
    }

    /**
     * Makes a string green.
     * @param in The string to make green.
     * @return The green string.
     */
    public static String green(String in) {
        return ConsoleEscape.FG_GREEN + in + ConsoleEscape.RESET;
    }

    /**
     * Makes a string blue.
     * @param in The string to make blue.
     * @return The blue string.
     */
    public static String blue(String in) {
        return ConsoleEscape.FG_BLUE + in + ConsoleEscape.RESET;
    }
}
