package superstore.util;

public enum ConsoleEscape {
    RESET, CLEAR, BG_BLACK, BG_RED, BG_GREEN, BG_YELLOW, BG_BLUE, BG_WHITE, FG_RED, FG_GREEN, FG_YELLOW, FG_BLUE, FX_UNDERLINE, FX_BOLD;

    public String toString() {
        switch(this){
            case RESET:        return "\033[0m";
            case CLEAR:        return "\033[H\033[2J";
            case BG_BLACK:     return "\033[40m";
            case BG_RED:       return "\033[30m\033[41m";
            case BG_GREEN:     return "\033[30m\033[42m";
            case BG_YELLOW:    return "\033[30m\033[43m";
            case BG_BLUE:      return "\033[30m\033[44m";
            case BG_WHITE:     return "\033[30m\033[47m";
            case FG_RED:       return "\033[31m";
            case FG_GREEN:     return "\033[32m";
            case FG_YELLOW:    return "\033[33m";
            case FG_BLUE:      return "\033[34m";
            case FX_BOLD:      return "\u001b[1m";
            case FX_UNDERLINE: return "\u001b[4m";
            default:           return "";
        }
    }
}