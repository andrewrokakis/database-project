package superstore.util;

import java.nio.charset.StandardCharsets;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.google.common.hash.Hashing;

import superstore.dbtypes.SuperstoreUser;

public class SuperstoreLogin {
    private Connection conn;
    private String email;
    private boolean isEmployee;
    private SuperstoreUser userInformation;

    /**
     * Constructor for the SuperstoreLogin
     * @param conn The database connection that will be used to login and run commands on.
     * @param email The email the user typed in when they were logging in.
     */
    public SuperstoreLogin(Connection conn, String loginType, String email) {
        this.conn = conn;
        this.isEmployee = loginType.equals("e");
        this.email = email;
    }

    /**
     * Checks if the logged in user is an employee or not.
     * @return True if they are an employee, false if they are a customer.
     */
    public boolean isEmployee() {
        return this.isEmployee;
    }

    /**
     * Returns an object with firstname, lastname, address and email of the logged in user.
     * @return Object containing user information.
     */
    public SuperstoreUser getUserInformation() {
        return this.userInformation;
    }

    /**
     * Returns the user's full name.
     * @return User's full name.
     */
    public String getFullName() {
        return this.userInformation.getFirstName() + " " + this.userInformation.getLastName();
    }

    /**
     * Returns the current database connection.
     * @return Database connection.
     */
    public Connection getConnection() {
        return this.conn;
    }

    /**
     * Returns the authentication salt for the user passed in the constructor.
     * @return String Salt
     * @throws SQLException If the database call was unsuccessful.
     */
    private String getSalt() throws SQLException {
        String sql = this.isEmployee ? 
            "{ ? = call authentication.get_employee_salt(?) }" : 
            "{ ? = call authentication.get_customer_salt(?) }";

        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.BINARY);
        stmt.setString(2, this.email);
        stmt.execute();

        String salt = new String(stmt.getBytes(1));
        return salt;
    }

    /**
     * Attempts to login as a customer with the given password.
     * It uses the username passed to the constructor.
     * If successful, the method will set the customerName field in this object.
     * @param password The password to login with.
     * @throws SQLException If the database call was unsuccessful.
     */
    public void attemptLogin(String password) throws SQLException {
        String salt = getSalt();
        byte[] hashedSaltedPassword = Hashing.sha256().hashString(salt + password, StandardCharsets.UTF_8).toString().getBytes();
        
        String sql = this.isEmployee ? 
            "{ ? = call authentication.validate_employee_authentication(?, ?) }" :
            "{ ? = call authentication.validate_customer_authentication(?, ?) }"; 

        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.STRUCT, "USER_TYPE");
        stmt.setString(2, this.email);
        stmt.setBytes(3, hashedSaltedPassword);
        stmt.execute();
        
        this.userInformation = (SuperstoreUser)stmt.getObject(1);
    }
}
