package superstore.cmds;

import java.sql.SQLException;

import superstore.util.SuperstoreLogin;

public class CommandRunner {
    /**
     * Will search the given command list for the given command code, and will execute it if found.
     * @param conn The object containing the information about the current user's login.
     * @param code The command code (inputted by the user) that we will search for and run.
     * @param commands The list of commands that the user can run at this time.
     * @return Returns true if a command was found and executed, otherwise returns false.
     */
    public static boolean attemptRunCommandFromShortcode(SuperstoreLogin conn, String code, ICommand[] commands) {
        // Process the globally-accessible commands (quit and back)
        if (code.equals("q")) {
            try {
                conn.getConnection().close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
        else if (code.equals("b")) {
            // Break out of the command loop that this was called from.
            return true;
        }
        else {
            // Search the commands list for the command matching the code
            for ( ICommand command : commands ) {
                if (!(command instanceof CommandViewerHeading) && command.getShortCode().equals(code)) {
                    // A command was found, execute it and then return true.
                    command.execute(conn);
                    return true;
                }
            }
        }
        // If  we reach here, no command was found.
        return false;
    }

    /**
     * Given an array of ICommand instances, it will display them in the console, then prompting the user to select one of them.
     * @param conn The object containing the information about the current user's login.
     * @param commands The list of commands that the user can run at this time.
     */
    public static void promptCommandFromList(SuperstoreLogin conn, ICommand[] commands) {
        System.out.println("");
        System.out.println("Here are some commands you can run:");
        for ( ICommand command : commands ) {
            if (command instanceof CommandViewerHeading) {
                // If the command is a heading instance, print it out as a heading.
                System.out.println("- " + command.getDescription());
            }
            else {
                // Print out the command code and command description.
                System.out.println("  [" + command.getShortCode() + "] " + command.getDescription());
            }
        }

        // These commands are always available, so they don't need to be passed in the commands argument.
        System.out.println("- Navigation");
        System.out.println("  [b] Go back");
        System.out.println("  [q] Exit the program");
        System.out.println("");

        while (true) {
            // Take command code from the stdin
            String cmd = System.console().readLine("Command: ").toLowerCase();

            // Attempt to run the command that the user just typed in
            boolean result = CommandRunner.attemptRunCommandFromShortcode(conn, cmd, commands);

            // If the result says that a command was successfully run, break out of this loop, or else keep prompting for a valid command.
            if (result == true) {
                break;
            }
        }
    }
}
