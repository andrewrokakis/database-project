package superstore.cmds;

import superstore.util.SuperstoreLogin;

/**
 * This class is used to display a heading in the command viewer.
 * It does not actually run any commands, and cannot be invoked by the user.
 */
public class CommandViewerHeading implements ICommand {
    private String heading;

    public CommandViewerHeading(String heading) {
        this.heading = heading;
    }

    @Override
    public String getShortCode() { return ""; }

    @Override
    public String getDescription() { return this.heading; }

    @Override
    public void execute(SuperstoreLogin conn) {}
    
}
