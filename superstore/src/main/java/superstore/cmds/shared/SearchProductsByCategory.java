package superstore.cmds.shared;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Search for all products whose category matches a given search query.
 */
public class SearchProductsByCategory implements ICommand {
    @Override
    public String getShortCode() { return "spc"; }

    @Override
    public String getDescription() { return "Search for products in a category"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Accept input
            String category = System.console().readLine("Category search query: ");

            // Run database command
            String sql = "{ ? = call store_customer_tools.search_products_by_category(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setString(2, category);
            stmt.execute();

            ResultSet cursor = stmt.getObject(1, ResultSet.class);

            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are the results we found for category \"" + category + "\":");
            System.out.println("");
            System.out.printf("%-4s%-24s%-15s%-7s%-11s%n", "Id", "Product", "Category", "Stars", "Total Stock");
            System.out.println("-------------------------------------------------------------");

            while (cursor.next()) {
                System.out.printf("%-4s%-24s%-15s%-9s%-11s%n",
                    cursor.getInt("product_id"),
                    cursor.getString("product_name"),
                    cursor.getString("category"),
                    cursor.getDouble("average_score"),
                    cursor.getInt("total_quantity_across_stores")
                );
            }
            System.out.println("-------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new ViewStoresWithProduct(),
                    new CommandViewerHeading("Manage"),
                    new AddProduct(),
                    new UpdateProduct(),
                    new DeleteProduct(),
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews()
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new ViewStoresWithProduct(),
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews(),
                    new AddReview(),
                    new CommandViewerHeading("Order"),
                    new PlaceOrder()
                });
            }
        } catch (SQLException e) {
            System.out.println("Cannot run command SearchProductsByCategory; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}



