package superstore.cmds.shared;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Get a list of stores that have a given product.
 */
public class ViewStoresWithProduct implements ICommand {
    @Override
    public String getShortCode() { return "vsp"; }

    @Override
    public String getDescription() { return "View stores with a given product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Accept input
            int productId;

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("b")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid product id."));
                }
            }

            // Run database command
            String sql = "{ ? = call store_customer_tools.view_stores_for_product(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setInt(2, productId);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the stores with this product:");
            System.out.println("");
            System.out.printf("%-4s%-24s%-15s%-10s%-8s%n", "Id", "Store", "Category", "Quantity", "Price");
            System.out.println("-------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%-15s%-10s%-8s%n",
                    cursor.getInt("store_id"),
                    cursor.getString("store_name"),
                    cursor.getString("category"),
                    cursor.getInt("quantity"),
                    cursor.getDouble("price")
                );
            }
            System.out.println("-------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new AddStore(),
                    new UpdateStore(),
                    new DeleteStore(),
                    new CommandViewerHeading("Stock"),
                    new AddStoreProduct(),
                    new UpdateStoreProduct()
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Order"),
                    new PlaceOrder(),
                });
            }
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewStoresWithProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
