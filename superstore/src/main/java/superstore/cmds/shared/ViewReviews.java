package superstore.cmds.shared;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View all of the reviews for a given product.
 */
public class ViewReviews implements ICommand {
    @Override
    public String getShortCode() { return "vr"; }

    @Override
    public String getDescription() { return "View reviews for a product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Accept input
            int productId;

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("b")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid product id."));
                }
            }

            // Run database command
            String sql = "{ ? = call store_customer_tools.view_reviews(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setInt(2, productId);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the reviews for this product:");
            System.out.println("");
            System.out.printf("%-4s%-36s%-6s%-48s%n", "Id", "Reviewer", "Stars", "Review");
            System.out.println("---------------------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-36s%-6s%-48s%n",
                    cursor.getInt("review_id"),
                    cursor.getString("reviewer"),
                    cursor.getInt("review"),
                    cursor.getString("review_description")
                );
            }
            System.out.println("---------------------------------------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();
            
            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Moderate"),
                    new DeleteReview()
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Flag"),
                    new FlagReview()
                });
            }
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewReviews; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
