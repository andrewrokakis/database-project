package superstore.cmds.shared;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View all stores.
 */
public class ViewStores implements ICommand {
    @Override
    public String getShortCode() { return "vs"; }

    @Override
    public String getDescription() { return "View all stores"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Run database command
            String sql = "{ ? = call store_customer_tools.view_stores() }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the stores:");
            System.out.printf("%-4s%-24s%n", "Id", "Store");
            System.out.println("----------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%n", cursor.getInt("store_id"), cursor.getString("store_name"));
            }
            System.out.println("----------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Manage"),
                    new AddStore(),
                    new UpdateStore(),
                    new DeleteStore(),
                    new CommandViewerHeading("Stock"),
                    new AddStoreProduct(),
                    new UpdateStoreProduct(),
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new ViewProductsInStore(),
                    new CommandViewerHeading("Order"),
                    new PlaceOrder()
                });
            }
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewStores; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
