package superstore.cmds.shared;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View all of the products available in a given store.
 */
public class ViewProductsInStore implements ICommand {
    @Override
    public String getShortCode() { return "vps"; }

    @Override
    public String getDescription() { return "View products in a given store"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Accept input
            int storeId;

            while (true) {
                String iStoreId = System.console().readLine("Store Id: ").toLowerCase();
                if (iStoreId.equals("")) return;
                try {
                    storeId = Integer.parseInt(iStoreId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid store id."));
                }
            }

            // Run database command
            String sql = "{ ? = call store_customer_tools.view_products_for_store(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setInt(2, storeId);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the available products in this store:");
            System.out.println("");
            System.out.printf("%-4s%-24s%-15s%-10s%-8s%n", "Id", "Product", "Category", "Quantity", "Price");
            System.out.println("-------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%-15s%-10s%-8s%n",
                    cursor.getInt("product_id"),
                    cursor.getString("product_name"),
                    cursor.getString("category"),
                    cursor.getInt("quantity"),
                    cursor.getDouble("price")
                );
            }
            System.out.println("-------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Manage"),
                    new AddProduct(),
                    new UpdateProduct(),
                    new DeleteProduct(),
                    new CommandViewerHeading("Stock"),
                    new AddStoreProduct(),
                    new UpdateStoreProduct(),
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews()
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews(),
                    new AddReview(),
                    new CommandViewerHeading("Order"),
                    new PlaceOrder()
                });
            }
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewProductsInStore; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
