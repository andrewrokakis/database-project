package superstore.cmds.shared;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;

import superstore.cmds.customer.*;
import superstore.cmds.employee.*;

import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Search for a given product across all stores.
 */
public class SearchProductsAcrossStores implements ICommand {
    @Override
    public String getShortCode() { return "sps"; }

    @Override
    public String getDescription() { return "Search for a product across stores"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            // Accept input
            String searchQuery = System.console().readLine("Search query: ");

            // Run database command
            String sql = "{ ? = call store_customer_tools.search_product_across_stores(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setString(2, searchQuery);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are the results we found for \"" + searchQuery + "\":");
            System.out.println("");
            System.out.printf("%-4s%-4s%-24s%-24s%-15s%-10s%-8s%n", "Sid", "Pid", "Store Name", "Product Name", "Category", "Quantity", "Price");
            System.out.println("-----------------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-4s%-24s%-24s%-15s%-10s%-8s%n",
                    cursor.getInt("store_id"),
                    cursor.getInt("product_id"),
                    cursor.getString("store_name"),
                    cursor.getString("product_name"),
                    cursor.getString("category"),
                    cursor.getInt("quantity"),
                    cursor.getDouble("price")
                );
            }
            System.out.println("-----------------------------------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            if (conn.isEmployee()) {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new ViewStoresWithProduct(),
                    new ViewProductsInStore(),
                    new CommandViewerHeading("Manage"),
                    new AddStoreProduct(),
                    new UpdateStoreProduct(),
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews()
                });
            }
            else {
                CommandRunner.promptCommandFromList(conn, new ICommand[] {
                    new CommandViewerHeading("Search"),
                    new ViewStoresWithProduct(),
                    new ViewProductsInStore(),
                    new CommandViewerHeading("Reviews"),
                    new ViewReviews(),
                    new AddReview(),
                    new CommandViewerHeading("Order"),
                    new PlaceOrder()
                });
            }
        }
        catch (SQLException e) {
            System.out.println("Cannot run command SearchProductsAcrossStores; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
