package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View reviews that were flagged as inappropriate by users.
 * For employees.
 */
public class ViewFlaggedReviews implements ICommand {
    @Override
    public String getShortCode() { return "vfr"; }

    @Override
    public String getDescription() { return "View reviews that were flagged as inappropriate"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Run database command
            String sql = "{ ? = call store_manager_tools.view_flagged_reviews() }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the flagged reviews:");
            System.out.println("");
            System.out.printf("%-4s%-6s%-36s%-24s%-6s%-48s%n", "Id", "Flag", "Reviewer", "Product", "Stars", "Review");
            System.out.println("---------------------------------------------------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-6s%-36s%-24s%-6s%-48s%n",
                    cursor.getInt("review_id"),
                    cursor.getInt("review_flag"),
                    cursor.getString("reviewer"),
                    cursor.getString("product_name"),
                    cursor.getInt("review"),
                    cursor.getString("review_description")
                );
            }
            System.out.println("---------------------------------------------------------------------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();
            
            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("Manage"),
                new DeleteReview()
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewFlaggedReviews; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
