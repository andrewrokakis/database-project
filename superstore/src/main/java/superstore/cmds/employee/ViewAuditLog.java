package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View the audit logs (changes made to important data).
 * For employees.
 */
public class ViewAuditLog implements ICommand {
    @Override
    public String getShortCode() { return "val"; }

    @Override
    public String getDescription() { return "View Audit Logs"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Run database command
            String sql = "{ ? = call store_manager_tools.view_audit_logs() }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are the audit logs:");
            System.out.println("");
            System.out.printf("%-4s%-28s%-24s%-10s%-80s%n", "Id", "Action Date","Table Name","Action", "Information");
            System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-28s%-24s%-10s%-80s%n",
                    cursor.getInt("log_id"),
                    cursor.getTimestamp("action_date"),
                    cursor.getString("table_name"),
                    cursor.getString("action"),
                    cursor.getString("information")
                );
            }
            System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] { });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewAuditLogs; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}

