package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Product;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Create a new product.
 * For employees.
 */
public class AddProduct implements ICommand {
    @Override
    public String getShortCode() { return "ap"; }

    @Override
    public String getDescription() { return "Add a new product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Accept input
            ConsoleHelper.clear();
            System.out.println("Add a product (press enter to cancel)");

            String productName = System.console().readLine("Product name: ");
            if (productName.equals("")) return;

            String productCategory = System.console().readLine("Product category: ");
            if (productCategory.equals("")) return;

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.add_product(?) }";
            Product newProd = new Product(productName, productCategory);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, newProd);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully created product " + productName + "."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command AddProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
