package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Search all of the warehouses for a given product.
 * For employees.
 */
public class SearchProductsAcrossWarehouses implements ICommand {
    @Override
    public String getShortCode() { return "spw"; }

    @Override
    public String getDescription() { return "Search for products across warehouses"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        try {
            String searchQuery = System.console().readLine("Search query: ");
            String sql = "{ ? = call store_manager_tools.search_product_across_warehouses(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setString(2, searchQuery);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            ConsoleHelper.clear();
            System.out.println("Here are the results we found for \"" + searchQuery + "\":");
            System.out.println("");
            System.out.printf("%-4s%-4s%-27s%-24s%-15s%-8s%n", "Wid", "Pid", "Warehouse Name", "Product Name", "Category", "Quantity");
            System.out.println("----------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-4s%-27s%-24s%-15s%-8s%n",
                    cursor.getInt("warehouse_id"),
                    cursor.getInt("product_id"),
                    cursor.getString("warehouse_name"),
                    cursor.getString("product_name"),
                    cursor.getString("category"),
                    cursor.getInt("quantity")
                );
            }

            cursor.close();
            stmt.close();

            System.out.println("----------------------------------------------------------------------------------");

            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("Stock"),
                new AddWarehouseProduct(),
                new UpdateWarehouseProduct()
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command SeaarchProductsAcrossWarehouses; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
