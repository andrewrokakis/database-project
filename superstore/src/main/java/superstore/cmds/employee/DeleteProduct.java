package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Delete a product.
 * For employees.
 */
public class DeleteProduct implements ICommand {
    @Override
    public String getShortCode() { return "dp"; }

    @Override
    public String getDescription() { return "Delete a product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Product Id to delete (press enter to cancel):");

            // Accept input
            int productId;

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid store id."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.delete_product(?, ?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setInt(1, productId);
            stmt.registerOutParameter(2, Types.VARCHAR);
            stmt.execute();
            
            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully deleted product " + stmt.getString(2) + "."));
            
            stmt.close();
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command DeleteProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
