package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Delete a store.
 * For employees.
 */
public class DeleteStore implements ICommand {
    @Override
    public String getShortCode() { return "ds"; }

    @Override
    public String getDescription() { return "Delete a store"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Store Id to delete (press enter to cancel):");

            // Accept input
            int storeId;

            while (true) {
                String iStoreId = System.console().readLine("Store Id: ").toLowerCase();
                if (iStoreId.equals("")) return;
                try {
                    storeId = Integer.parseInt(iStoreId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid store id."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.delete_store(?, ?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setInt(1, storeId);
            stmt.registerOutParameter(2, Types.VARCHAR);
            stmt.execute();
            
            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully deleted store " + stmt.getString(2) + "."));
            
            stmt.close();
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command DeleteStore; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
