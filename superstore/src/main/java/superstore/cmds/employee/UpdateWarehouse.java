package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Warehouse;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Update a warehouse's information.
 * For employees.
 */
public class UpdateWarehouse implements ICommand {
    @Override
    public String getShortCode() { return "uw"; }

    @Override
    public String getDescription() { return "Update a warehouse's information"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Warehouse Id to update (press enter to cancel):");

            // Accept input
            int warehouseId;

            while (true) {
                String iWarehouseId = System.console().readLine("Warehouse Id: ").toLowerCase();
                if (iWarehouseId.equals("")) return;
                try {
                    warehouseId = Integer.parseInt(iWarehouseId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid warehouse id."));
                }
            }

            // Run and cleanup database command
            String warehouseName = System.console().readLine("New Warehouse Name: ");
            if (warehouseName.equals("")) return;

            String sql = "{ call store_manager_tools.update_warehouse(?) }";
            Warehouse updatedWarehouse = new Warehouse(warehouseId, warehouseName);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, updatedWarehouse);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully updated warehouse."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command UpdateWarehouse; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}