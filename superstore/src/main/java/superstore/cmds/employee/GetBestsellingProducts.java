package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.shared.*;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Get the bestselling products.
 * For employees.
 */
public class GetBestsellingProducts implements ICommand {
    @Override
    public String getShortCode() { return "vpp"; }

    @Override
    public String getDescription() { return "View popular products"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Run database command
            String sql = "{ ? = call store_manager_tools.show_most_popular_products() }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are the bestselling products:");
            System.out.println("");
            System.out.printf("%-4s%-24s%-15s%-9s%-7s%n", "Id", "Product", "Category", "Orders", "Stars");
            System.out.println("-----------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%-15s%-9s%-7s%n",
                    cursor.getInt("product_id"),
                    cursor.getString("product_name"),
                    cursor.getString("category"),
                    cursor.getInt("order_count"),
                    cursor.getDouble("average_score")
                );
            }
            System.out.println("-----------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("View"),
                new ViewStoresWithProduct(),
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command GetBestsellingProducts; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
