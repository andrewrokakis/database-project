package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Delete a warehouse.
 * For employees.
 */
public class DeleteWarehouse implements ICommand {
    @Override
    public String getShortCode() { return "dw"; }

    @Override
    public String getDescription() { return "Delete a warehouse"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Warehouse Id to delete (press enter to cancel):");

            // Accept input
            int warehouseId;

            while (true) {
                String iWarehouseId = System.console().readLine("Warehouse Id: ").toLowerCase();
                if (iWarehouseId.equals("")) return;
                try {
                    warehouseId = Integer.parseInt(iWarehouseId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid warehouse id."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.delete_warehouse(?, ?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setInt(1, warehouseId);
            stmt.registerOutParameter(2, Types.VARCHAR);
            stmt.execute();
            
            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully deleted warehouse " + stmt.getString(2) + "."));

            stmt.close();
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command DeleteWarehouse; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
