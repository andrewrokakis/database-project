package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Warehouse;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Create a new warehouse.
 * For employees.
 */
public class AddWarehouse implements ICommand {
    @Override
    public String getShortCode() { return "aw"; }

    @Override
    public String getDescription() { return "Add a new warehouse"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            ConsoleHelper.clear();
            System.out.println("Add a warehouse (press enter to cancel)");

            // Accept input
            String warehouseName = System.console().readLine("Warehouse name: ");
            if (warehouseName.equals("")) {
                return;
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.add_warehouse(?) }";
            Warehouse newWarehouse = new Warehouse(warehouseName);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, newWarehouse);
            stmt.execute();
            stmt.close();
            
            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully created warehouse " + warehouseName + "."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command AddWarehouse; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
