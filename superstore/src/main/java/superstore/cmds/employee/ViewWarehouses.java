package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View all warehouses.
 * For employees.
 */
public class ViewWarehouses implements ICommand {
    @Override
    public String getShortCode() { return "vw"; }

    @Override
    public String getDescription() { return "View all warehouses"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Run database command
            String sql = "{ ? = call store_manager_tools.view_warehouses() }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the warehouses:");
            System.out.println("");
            System.out.printf("%-4s%-24s%n", "Id", "Warehouse");
            System.out.println("----------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%n", cursor.getInt("warehouse_id"), cursor.getString("warehouse_name"));
            }
            System.out.println("----------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("View"),
                new ViewProductsInWarehouse(),
                new CommandViewerHeading("Manage"),
                new AddWarehouse(),
                new UpdateWarehouse(),
                new DeleteWarehouse(),
                new CommandViewerHeading("Stock"),
                new AddWarehouseProduct(),
                new UpdateWarehouseProduct(),
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewWarehouses; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
