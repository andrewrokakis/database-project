package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Delete a review.
 * For employees.
 */
public class DeleteReview implements ICommand {
    @Override
    public String getShortCode() { return "dr"; }

    @Override
    public String getDescription() { return "Delete a review"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Review Id to delete (press enter to cancel):");

            // Accept input
            int reviewId;

            while (true) {
                String iReviewId = System.console().readLine("Review Id: ").toLowerCase();
                if (iReviewId.equals("")) return;
                try {
                    reviewId = Integer.parseInt(iReviewId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid review id."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.delete_review(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully deleted review."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command DeleteReview; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
