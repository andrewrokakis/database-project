package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * View all of the products in a given warehouse.
 * For employees.
 */
public class ViewProductsInWarehouse implements ICommand {
    @Override
    public String getShortCode() { return "vpw"; }

    @Override
    public String getDescription() { return "View all products in a warehouse"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Accept input
            int warehouseId;

            while (true) {
                String iWarehouseId = System.console().readLine("Warehouse Id: ").toLowerCase();
                if (iWarehouseId.equals("b")) return;
                try {
                    warehouseId = Integer.parseInt(iWarehouseId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid warehouse id."));
                }
            }

            // Run database command
            String sql = "{ ? = call store_manager_tools.view_products_for_warehouse(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setInt(2, warehouseId);
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of the available products:");
            System.out.println("");
            System.out.printf("%-4s%-24s%-15s%-8s%n", "Id", "Name", "Category", "Quantity");
            System.out.println("---------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%-15s%-8s%n", cursor.getInt("product_id"), cursor.getString("product_name"), cursor.getString("category"), cursor.getInt("quantity"));
            }
            System.out.println("---------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();

            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("Manage"),
                new AddProduct(),
                new UpdateProduct(),
                new DeleteProduct(),
                new CommandViewerHeading("Stock"),
                new AddWarehouseProduct(),
                new UpdateWarehouseProduct()
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewProductsInWarehouse; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
