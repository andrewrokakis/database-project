package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.WarehouseProduct;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Update the information of a product in a warehouse.
 * For employees.
 */
public class UpdateWarehouseProduct implements ICommand {
    @Override
    public String getShortCode() { return "uwp"; }

    @Override
    public String getDescription() { return "Update the information of a product in a warehouse"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter the information to update (press enter to cancel):");

            // Accept input
            int warehouseId;
            int productId;
            int quantity;

            while (true) {
                String iWarehouseId = System.console().readLine("Warehouse Id: ").toLowerCase();
                if (iWarehouseId.equals("")) return;
                try {
                    warehouseId = Integer.parseInt(iWarehouseId);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid warehouse id.")); }
            }

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid product id.")); }
            }

            while (true) {
                String iQuantity = System.console().readLine("New Quantity: ").toLowerCase();
                if (iQuantity.equals("")) return;
                try {
                    quantity = Integer.parseInt(iQuantity);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid quantity.")); }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.update_warehouse_product(?) }";
            WarehouseProduct updatedProduct = new WarehouseProduct(warehouseId, productId, quantity);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, updatedProduct);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully updated warehouse product."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command UpdateWarehouseProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}