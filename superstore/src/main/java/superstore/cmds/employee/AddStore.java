package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Store;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Create a new store.
 * For employees.
 */
public class AddStore implements ICommand {
    @Override
    public String getShortCode() { return "as"; }

    @Override
    public String getDescription() { return "Add a new store"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            ConsoleHelper.clear();
            System.out.println("Add a store (press enter to cancel)");

            // Accept input
            String storeName = System.console().readLine("Store name: ");
            if (storeName.equals("")) { return; }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.add_store(?) }";
            Store newStore = new Store(storeName);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, newStore);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully created store " + storeName + "."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command AddStore; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
