package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Store;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Update a store's information.
 * For employees.
 */
public class UpdateStore implements ICommand {
    @Override
    public String getShortCode() { return "us"; }

    @Override
    public String getDescription() { return "Update a store's information"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Store Id to update (press enter to cancel):");

            // Accept input
            int storeId;

            while (true) {
                String iStoreId = System.console().readLine("Store Id: ").toLowerCase();
                if (iStoreId.equals("")) return;
                try {
                    storeId = Integer.parseInt(iStoreId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid store id."));
                }
            }

            // Run and cleanup database command
            String storeName = System.console().readLine("New Store Name: ");
            if (storeName.equals("")) return;

            String sql = "{ call store_manager_tools.update_store(?) }";
            Store updatedStore = new Store(storeId, storeName);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, updatedStore);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully updated store."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command UpdateStore; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}