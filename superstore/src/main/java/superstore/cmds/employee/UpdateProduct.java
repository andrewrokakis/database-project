package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Product;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Update a product's information.
 * For employees.
 */
public class UpdateProduct implements ICommand {
    @Override
    public String getShortCode() { return "up"; }

    @Override
    public String getDescription() { return "Update a product information"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter a Product Id to update (press enter to cancel, or - to skip a given field):");

            // Accept input
            int productId;

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid product id."));
                }
            }

            String productName = System.console().readLine("New Product Name: ");
            if (productName.equals("")) return;

            String productCategory = System.console().readLine("New Product Category: ");
            if (productCategory.equals("")) return;

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.update_product(?) }";
            Product updatedProduct = new Product(productId, productName, productCategory);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, updatedProduct);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully updated product."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command UpdateProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}