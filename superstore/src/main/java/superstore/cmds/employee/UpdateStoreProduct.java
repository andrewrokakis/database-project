package superstore.cmds.employee;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.StoreProduct;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Update the information of a product in a store.
 * For employees.
 */
public class UpdateStoreProduct implements ICommand {
    @Override
    public String getShortCode() { return "usp"; }

    @Override
    public String getDescription() { return "Update the information of a product in a store"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent customers from running employee-only command
        if (!conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter the information to update (press enter to cancel, or - to skip a given field):");

            // Accept input
            int storeId;
            int productId;
            int quantity;
            double price;

            while (true) {
                String iStoreId = System.console().readLine("Store Id: ").toLowerCase();
                if (iStoreId.equals("")) return;
                try {
                    storeId = Integer.parseInt(iStoreId);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid store id.")); }
            }

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid product id.")); }
            }

            while (true) {
                String iQuantity = System.console().readLine("New Quantity: ").toLowerCase();
                if (iQuantity.equals("")) return;
                if (iQuantity.equals("-")) {
                    quantity = -1;
                    break;
                }
                try {
                    quantity = Integer.parseInt(iQuantity);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid quantity.")); }
            }

            while (true) {
                String iPrice = System.console().readLine("New Price: ").toLowerCase();
                if (iPrice.equals("")) return;
                if (iPrice.equals("-")) {
                    price = -1;
                    break;
                }
                try {
                    price = Double.parseDouble(iPrice);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid price.")); }
            }

            // Run and cleanup database command
            String sql = "{ call store_manager_tools.update_store_product(?) }";
            StoreProduct updatedProduct = new StoreProduct(storeId, productId, quantity, price);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, updatedProduct);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully updated store product."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command UpdateStoreProduct; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}