package superstore.cmds.customer;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Order;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Command to place an order on a product in a given store.
 * For customers.
 */
public class PlaceOrder implements ICommand {
    @Override
    public String getShortCode() { return "or"; }

    @Override
    public String getDescription() { return "Place an order on a product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent employees from running customer-only command
        if (conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter the information required to place your order (press enter to cancel):");
            
            // Accept input
            int storeId;
            int productId;
            int quantity;

            while (true) {
                String iStoreId = System.console().readLine("Store Id: ").toLowerCase();
                if (iStoreId.equals("")) return;
                try {
                    storeId = Integer.parseInt(iStoreId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid store id."));
                }
            }

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid product id."));
                }
            }

            while (true) {
                String iQuantity = System.console().readLine("Quantity: ").toLowerCase();
                if (iQuantity.equals("")) return;
                try {
                    quantity = Integer.parseInt(iQuantity);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid quantity."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_customer_tools.place_order(?) }";
            Order newOrder = new Order(conn.getUserInformation().getId(), storeId, productId, quantity);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, newOrder);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully placed order."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command PlaceOrder; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
