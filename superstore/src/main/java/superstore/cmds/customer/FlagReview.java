package superstore.cmds.customer;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Command to flag a review as inappropriate.
 * For customers.
 */
public class FlagReview implements ICommand {
    @Override
    public String getShortCode() { return "fr"; }

    @Override
    public String getDescription() { return "Flag a review as inappropriate"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent employees from running customer-only command
        if (conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("");
            System.out.println("Enter the id of the review you want to flag (press enter to cancel):");

            // Accept input
            int reviewId;

            while (true) {
                String iReviewId = System.console().readLine("Review Id: ").toLowerCase();
                if (iReviewId.equals("")) return;
                try {
                    reviewId = Integer.parseInt(iReviewId);
                    break;
                }
                catch (NumberFormatException e) {
                    System.out.println(ConsoleHelper.red("Please enter a valid review id."));
                }
            }

            // Run and cleanup database command
            String sql = "{ call store_customer_tools.flag_review(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully flagged review."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command FlagReview; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
