package superstore.cmds.customer;

import java.sql.CallableStatement;
import java.sql.SQLException;

import superstore.cmds.ICommand;
import superstore.dbtypes.Review;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Add a review to a product.
 * For customers.
 */
public class AddReview implements ICommand {
    @Override
    public String getShortCode() { return "ar"; }

    @Override
    public String getDescription() { return "Add a review to a product"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent employees from running customer-only command
        if (conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Command setup
            System.out.println("Add a review to a product (press enter to cancel EXCEPT for review description)");

            // Accept input
            int productId;
            int review;
            String description;

            while (true) {
                String iProductId = System.console().readLine("Product Id: ").toLowerCase();
                if (iProductId.equals("")) return;
                try {
                    productId = Integer.parseInt(iProductId);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid product id.")); }
            }

            while (true) {
                String iReview = System.console().readLine("Review (1-5): ").toLowerCase();
                if (iReview.equals("")) return;
                try {
                    review = Integer.parseInt(iReview);
                    break;
                }
                catch (NumberFormatException e) { System.out.println(ConsoleHelper.red("Please enter a valid review.")); }
            }

            description = System.console().readLine("Describe your review: ");
            if (description.equals("")) description = null;

            // Run and cleanup database command
            String sql = "{ call store_customer_tools.add_review(?) }";
            Review newReview = new Review(conn.getUserInformation().getId(), productId, review, description);

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.setObject(1, newReview);
            stmt.execute();
            stmt.close();

            // Display success
            System.out.println("");
            System.out.println(ConsoleHelper.green("Successfully added review to product."));
            System.console().readLine();
        }
        catch (SQLException e) {
            System.out.println("Cannot run command AddReview; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}