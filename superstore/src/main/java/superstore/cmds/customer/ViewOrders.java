package superstore.cmds.customer;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import superstore.cmds.CommandRunner;
import superstore.cmds.CommandViewerHeading;
import superstore.cmds.ICommand;
import superstore.dbutil.SqlErrorCodeParser;
import superstore.util.ConsoleHelper;
import superstore.util.SuperstoreLogin;

/**
 * Command to show the user their order history.
 * FOr customers.
 */
public class ViewOrders implements ICommand {
    @Override
    public String getShortCode() { return "mo"; }

    @Override
    public String getDescription() { return "Show my orders"; }

    @Override
    public void execute(SuperstoreLogin conn) {
        // Prevent employees from running customer-only command
        if (conn.isEmployee()) {
            System.out.println(ConsoleHelper.red("Unauthorized."));
            return;
        }

        try {
            // Run database command
            String sql = "{ ? = call store_customer_tools.view_orders(?) }";

            CallableStatement stmt = conn.getConnection().prepareCall(sql);
            stmt.registerOutParameter(1, Types.REF_CURSOR);
            stmt.setInt(2, conn.getUserInformation().getId());
            stmt.execute();
            
            ResultSet cursor = stmt.getObject(1, ResultSet.class);
            
            // Display results
            ConsoleHelper.clear();
            System.out.println("Here are all of your orders:");
            System.out.println("");
            System.out.printf("%-4s%-24s%-24s%-10s%-10s%-12s%n", "Id", "Store", "Product", "Quantity", "Price", "Date");
            System.out.println("------------------------------------------------------------------------------------");

            while(cursor.next()) {
                System.out.printf("%-4s%-24s%-24s%-10s%-10s%-12s%n",
                    cursor.getInt("order_id"),
                    cursor.getString("store_name"),
                    cursor.getString("product_name"),
                    cursor.getInt("quantity"),
                    cursor.getDouble("price"),
                    cursor.getDate("order_date").toLocalDate().toString()
                );
            }
            System.out.println("------------------------------------------------------------------------------------");

            // Cleanup
            cursor.close();
            stmt.close();
           
            // Show commands related to this command that the user can run.
            CommandRunner.promptCommandFromList(conn, new ICommand[] {
                new CommandViewerHeading("Reviews"),
                new AddReview()            
            });
        }
        catch (SQLException e) {
            System.out.println("Cannot run command ViewOrders; " + ConsoleHelper.red(SqlErrorCodeParser.getHumanReadableError(e)));
            System.console().readLine();
        }
    }
}
