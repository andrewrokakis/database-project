package superstore.cmds;

import superstore.util.SuperstoreLogin;

/**
 * The interface representing a user command.
 * The input handling for the commands is done inside of the execute method.
 * The execution of a command has internal exception handling and will display
 *  an appropriate message in the console without interrupting the program.
 */
public interface ICommand {
    /**
     * Gets the code that the user will enter into the console to run the given command
     * @return The command code
     */
    String getShortCode();

    /**
     * The command description that will be displayed in the console
     * @return The command's description
     */
    String getDescription();
    
    /**
     * The function that executes the command.
     * It will process all input/output that it requires.
     * @param conn The SuperstoreLogin containing all the information about the logged in superstore user
     */
    void execute(SuperstoreLogin conn);
}
