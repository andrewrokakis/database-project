-- #######################################
-- ##          CREATE TABLES            ##
-- #######################################

CREATE TABLE employees (
    employee_id     NUMBER(6, 0)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    firstname       VARCHAR2(50)    NOT NULL,
    lastname        VARCHAR2(50)    NOT NULL,
    address         VARCHAR2(100)   NOT NULL,
    email           VARCHAR2(100)   NOT NULL
);

CREATE TABLE customers (
    customer_id     NUMBER(6, 0)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    firstname       VARCHAR2(50)    NOT NULL,
    lastname        VARCHAR2(50)    NOT NULL,
    address         VARCHAR2(100),
    email           VARCHAR2(100)   NOT NULL
);

CREATE TABLE warehouses (
    warehouse_id    NUMBER(6, 0)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    warehouse_name  VARCHAR2(100)   NOT NULL
);

CREATE TABLE stores (
    store_id        NUMBER(6, 0)    GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    store_name      VARCHAR2(100)   NOT NULL
);

CREATE TABLE products (
    product_id       NUMBER(6, 0)   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    product_name     VARCHAR2(100)  NOT NULL,
    category         VARCHAR2(100)  NOT NULL
);

CREATE TABLE reviews (
    review_id          NUMBER(6, 0)   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    customer_id        NUMBER(6, 0)   REFERENCES customers(customer_id) ON DELETE CASCADE,
    product_id         NUMBER(6, 0)   REFERENCES products(product_id) ON DELETE CASCADE,
    review             NUMBER(1, 0)   NOT NULL,
    review_flag        NUMBER(1, 0)   DEFAULT 0,
    review_description VARCHAR2(1000)
);

CREATE TABLE orders (
    order_id    NUMBER(6, 0) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    customer_id NUMBER(6, 0) REFERENCES customers(customer_id) ON DELETE CASCADE,
    store_id    NUMBER(6, 0) REFERENCES stores(store_id) ON DELETE CASCADE,
    product_id  NUMBER(6, 0) REFERENCES products(product_id) ON DELETE CASCADE,
    quantity    NUMBER(6, 0) NOT NULL,
    price       NUMBER(8, 2) NOT NULL,
    order_date  DATE         NOT NULL
);

CREATE TABLE store_products (
    store_id   NUMBER(6, 0) REFERENCES stores(store_id) ON DELETE CASCADE,
    product_id NUMBER(6, 0) REFERENCES products(product_id) ON DELETE CASCADE,
    quantity   NUMBER(6, 0) NOT NULL,
    price      NUMBER(8, 2) NOT NULL
);

CREATE TABLE warehouse_products (
    warehouse_id NUMBER(6, 0) REFERENCES warehouses(warehouse_id) ON DELETE CASCADE,
    product_id   NUMBER(6, 0) REFERENCES products(product_id) ON DELETE CASCADE,
    quantity     NUMBER(6, 0) NOT NULL
);

CREATE TABLE authentication_salt (
    salt_id NUMBER(6, 0) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    salt    RAW(100)     NOT NULL
);

CREATE TABLE customer_authentication (
    customer_id NUMBER(6, 0) REFERENCES customers(customer_id) ON DELETE CASCADE,
    password    RAW(100)     NOT NULL,
    salt_id     NUMBER(6, 0) REFERENCES authentication_salt(salt_id) ON DELETE CASCADE
);

CREATE TABLE employee_authentication (
    employee_id NUMBER(6, 0) REFERENCES employees(employee_id) ON DELETE CASCADE,
    password    RAW(100)     NOT NULL,
    salt_id     NUMBER(6, 0) REFERENCES authentication_salt(salt_id) ON DELETE CASCADE
);

CREATE TABLE audit_log (
	log_id      NUMBER(6, 0) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	action_date TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP,
	table_name  VARCHAR2(64) NOT NULL,
	action      VARCHAR2(6)  NOT NULL,
	information VARCHAR2(2048)
);

CREATE TABLE audit_log_auth (
    log_id      NUMBER(6, 0)  GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    action_date TIMESTAMP(6)  DEFAULT CURRENT_TIMESTAMP,
    user_type   VARCHAR2(8)   NOT NULL,
    input       VARCHAR2(128) NOT NULL,
    action      VARCHAR2(64)  NOT NULL,
    description VARCHAR2(256)
);

-- #######################################
-- ##           INSERT DATA             ##
-- #######################################

INSERT INTO employees (firstname, lastname, address, email) VALUES ('Jerry',  'Seinfeld',   '2242 Carlson Road, Prince George, BC, Canada',               'jseinfeld@gmail.com');
INSERT INTO employees (firstname, lastname, address, email) VALUES ('George', 'Washington', '1600 Pennsylvania Avenue NW, Washington, DC, United States', 'potus@whitehouse.gov');
INSERT INTO employees (firstname, lastname, address, email) VALUES ('John',   'Doe',        '2639 Sherbrooke Ouest, Montreal, QC, Canada',                'a@a.com');

INSERT INTO customers (firstname, lastname, address, email) VALUES ('Mahsa',  'Sadeghi',   'dawson college, montreal, qeuebe, canada',         'msadeghi@dawsoncollege.qc.ca');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Alex',   'Brown',     '090 boul saint laurent, montreal, quebec, canada', 'alex@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Martin', 'alexandre', 'brossard, quebec, canada',                         'marting@yahoo.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Daneil', 'hanne',     '100 atwater street, toronto, canada',              'daneil@yahoo.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('John',   'boura',     '100 Young street, toronto, canada',                'bdoura@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Ari',    'brown',      NULL,                                              'b.a@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Amanda', 'Harry',     '100 boul saint laurent, montreal, quebec, canada', 'am.harry@yahioo.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Jack',   'Jonhson',   'Calgary, Alberta, Canada',                         'johnson.a@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Mahsa',  'sadeghi',   '104 gill street, Toronto, Canada',                 'ms@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('John',   'belle',     '105 Young street, toronto, canada',                'abcd@yahoo.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Martin', 'Li',        '87 boul saint laurent, montreal, quebec, canada',  'm.li@gmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Olivia', 'smith',     '76 boul decalthon, laval, quebec, canada',         'smith@hotmail.com');
INSERT INTO customers (firstname, lastname, address, email) VALUES ('Noah',   'Garcia',    '22222 happy street, Laval, quebec, canada',        'g.noah@yahoo.com');

INSERT INTO products (product_name, category) VALUES ('Laptop ASUS 104S',   'Electronics');
INSERT INTO products (product_name, category) VALUES ('Apple',              'Grocery');
INSERT INTO products (product_name, category) VALUES ('SIMS CD',            'Video Games');
INSERT INTO products (product_name, category) VALUES ('orange',             'Grocery');
INSERT INTO products (product_name, category) VALUES ('Barbie Movie',       'DVD');
INSERT INTO products (product_name, category) VALUES ('LOreal Normal Hair', 'Health');
INSERT INTO products (product_name, category) VALUES ('BMW iX Lego',        'Toys');
INSERT INTO products (product_name, category) VALUES ('BMW i6',             'Cars');
INSERT INTO products (product_name, category) VALUES ('Truck 500c',         'Vehicle');
INSERT INTO products (product_name, category) VALUES ('Paper Towel',        'Beauty');
INSERT INTO products (product_name, category) VALUES ('Plum',               'Grocery');
INSERT INTO products (product_name, category) VALUES ('Lamborghini Lego',   'Toys');
INSERT INTO products (product_name, category) VALUES ('Chicken',            'Grocery');
INSERT INTO products (product_name, category) VALUES ('Pasta',              'Grocery');
INSERT INTO products (product_name, category) VALUES ('PS5',                'Electronics');
INSERT INTO products (product_name, category) VALUES ('Tomato',             'Grocery');
INSERT INTO products (product_name, category) VALUES ('Train X745',         'Toys');

INSERT INTO stores (store_name) VALUES ('Marche Adonis');
INSERT INTO stores (store_name) VALUES ('Marche Atwater');
INSERT INTO stores (store_name) VALUES ('Dawson Store');
INSERT INTO stores (store_name) VALUES ('Store Magic');
INSERT INTO stores (store_name) VALUES ('Movie Store');
INSERT INTO stores (store_name) VALUES ('Super Rue Champlain');
INSERT INTO stores (store_name) VALUES ('Toys R Us');
INSERT INTO stores (store_name) VALUES ('Dealer One');
INSERT INTO stores (store_name) VALUES ('Dealer Montreal');
INSERT INTO stores (store_name) VALUES ('Movie Start');
INSERT INTO stores (store_name) VALUES ('Star Store');

INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse A');
INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse B');
INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse C');
INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse D');
INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse E');
INSERT INTO warehouses (warehouse_name) VALUES ('Warehouse F');

INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (1,  1,  4, 0,  'it was affordable.');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (2,  2,  3, 0,  'quality was not good');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (3,  3,  2, 1,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (4,  4,  5, 0,  'highly recommend');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (2,  5,  1, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (3,  6,  1, 0,  'did not worth the price');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (1,  7,  1, 0,  'missing some parts');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (5,  8,  5, 1,  'trash');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (6,  9,  2, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (7,  10, 5, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (8,  11, 4, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (3,  6,  3, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (1,  12, 1, 0,  'missing some parts');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (1,  11, 4, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (9,  12, 1, 0,  'great product');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (10, 8,  5, 1,  'bad quality');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (2,  3,  1, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (2,  5,  4, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (11, 13, 4, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (12, 14, 5, 0,  NULL);
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (1,  7,  1, 2,  'worse car i have droven!');
INSERT INTO reviews (customer_id, product_id, review, review_flag, review_description) VALUES (12, 14, 4, 0,  NULL);

INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (1,  1,  13, 1, 970,    '21-Apr-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (2,  2,  2,  2, 10,     '23-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (3,  3,  15, 3, 50,     '01-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (4,  4,  13, 1, 2,      '23-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (2,  5,  2,  1, 30,     '23-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (3,  6,  13, 1, 10,     '10-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (1,  7,  8,  1, 40,     '11-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (5,  8,  8,  1, 50000,  '10-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (6,  9,  15, 1, 856600, '24-Jan-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (7,  10, 13, 3, 50,     '12-Feb-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (8,  2,  14, 6, 10,     '06-May-20');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (3,  6,  13, 3, 30,     '12-Sep-19');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (1,  7,  12, 1, 40,     '11-Oct-10');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (1,  2,  14, 7, 10,     '06-May-22');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (9,  7,  12, 2, 80,     '07-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (10, 8,  8,  1, 50000,  '10-Aug-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (2,  5,  15, 1, 16,     '23-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (2,  7,  2,  1, 45,     '02-Oct-23');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (11, 1,  8,  1, 9,      '03-Apr-19');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (12, 2,  14, 3, 13,     '29-Dec-21');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (13, 11, 15, 1, 200,    '20-Jan-20');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (1,  7,  8,  1, 38,     '11-Oct-22');
INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date) VALUES (12, 4,  14, 3, 15,     '29-Dec-21');

INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(1, 1,  1000);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(2, 2,  24980);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(3, 3,  103);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(4, 4,  35405);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(5, 5,  40);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(6, 6,  450);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(1, 7,  10);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(1, 8,  6);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(5, 9,  1000);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(6, 10, 3532);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(3, 11, 43242);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(2, 10, 39484);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(4, 11, 6579);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(5, 12, 98765);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(6, 13, 43523);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(1, 14, 2132);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(4, 15, 123);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(1, 16, 352222);
INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES(5, 17, 4543);

INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  2,  7,  96.5);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  4,  20, 64.37);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  10, 25, 2.15);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  11, 34, 2.62);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  13, 8,  90.56);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (1,  14, 28, 17.95);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  2,  10, 66.42);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  4,  41, 11.56);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  10, 39, 82.48);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  11, 41, 18.11);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  13, 29, 48.98);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (2,  14, 4,  64.55);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (3,  1,  44, 83.88);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (3,  3,  35, 2.01);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (3,  5,  8,  86.02);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (3,  12, 22, 23.17);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (3,  15, 10, 71.06);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  1,  9,  82.03);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  3,  48, 81.63);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  5,  26, 20.65);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  6,  18, 96.66);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  7,  45, 55.64);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  12, 42, 96.34);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (4,  15, 45, 23.57);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (5,  5,  36, 87.31);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  2,  6,  85.27);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  4,  38, 45.68);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  10, 38, 51.1);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  11, 6,  20.65);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  13, 20, 78.46);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (6,  14, 17, 1.46);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  3,  38, 40.08);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  5,  17, 46.99);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  7,  28, 1.14);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  9,  33, 74.49);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  12, 45, 75.85);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (7,  15, 8,  4.09);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (8,  7,  6,  13.92);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (8,  8,  48, 78.44);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (8,  9,  23, 82.91);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (8,  12, 35, 64.22);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (9,  7,  42, 16.63);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (9,  8,  31, 10.09);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (9,  9,  5,  63.93);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (9,  12, 26, 8.82);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (10, 3,  20, 8.06);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (10, 5,  21, 75.57);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (11, 1,  11, 76.1);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (11, 6,  33, 79.96);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (11, 10, 6,  39.69);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (11, 14, 14, 73.71);
INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (11, 15, 47, 56.72);

INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('6f7c0f02'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('a82c88c0'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('0086ac8b'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('10aaa0a0'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('ded370a6'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('8c8032e9'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('22e9b394'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('70ec9376'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('51363bff'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('e5cdda41'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('a26f90bf'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('1050add5'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('953ab900'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('9e5bb76a'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('47795c9b'));
INSERT INTO authentication_salt (salt) VALUES (utl_raw.cast_to_raw('b7331f53'));

INSERT INTO employee_authentication (employee_id, password, salt_id) VALUES (1,  utl_raw.cast_to_raw('57ed273df77b38d2f169aa834f39d5c35fc2453b9a613457a422c78969039b19'), 1);
INSERT INTO employee_authentication (employee_id, password, salt_id) VALUES (2,  utl_raw.cast_to_raw('6aef59d45941af4779486df4001bd6e90a0bef848212b0c09d627627b9530e85'), 2);
INSERT INTO employee_authentication (employee_id, password, salt_id) VALUES (3,  utl_raw.cast_to_raw('dd1c2af7a90fa953ae2d0f1ec2ba5c6ce602cd39112264a5e1f3604e73e2307c'), 3);

INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (1,  utl_raw.cast_to_raw('be9e2c346f76498e613ce5190cfe8d30be428ad8e71fdf8cc19494d9717572ec'), 4);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (2,  utl_raw.cast_to_raw('88fe035657831e97a78462bf0ede145313a3c86ca0331c4bfd1bba5f59ac5dbd'), 5);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (3,  utl_raw.cast_to_raw('539c603b81b2998650c3f1990637f5034d67d42ab977980275a5fa85b50cc40b'), 6);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (4,  utl_raw.cast_to_raw('ba9b0e344b68a420feccbb4e5b95e67af48d4de777fe077a3776681d75ff5f5c'), 7);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (5,  utl_raw.cast_to_raw('909e8d89e908c65ab40d50580bdb8666c4c8dfb3b00b17b20aa15d670e81f8b1'), 8);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (6,  utl_raw.cast_to_raw('b9ed3c3078f860ee7752f5cbd13189536bb888654f1e3c878b115e12b19aa7e0'), 9);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (7,  utl_raw.cast_to_raw('4790b1b7a7b4d34262936f589d4ae3b4f4de9eb540d35ce52d1c4ee0e5e31c17'), 10);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (8,  utl_raw.cast_to_raw('e7ab821a2f383741a0b11952d4c952f86d164479d586f55b0f48a23fefd7c2ad'), 11);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (9,  utl_raw.cast_to_raw('b50186285fac0ac7c53a414e36b48be41d1cecd697bdc890f1dc75435ca7972f'), 12);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (10, utl_raw.cast_to_raw('ca3cc02d20ad0130c6aa7f89e620a8c85383db408aed873971fcd8eb22869716'), 13);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (11, utl_raw.cast_to_raw('5cfe0735caf4340be4113cf25f25a9cda919fa6318b062a74bb9a681694df8ac'), 14);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (12, utl_raw.cast_to_raw('a4b23ab9b375128ea121e4bc2afa3ea536a047fac03544d07835e23669833567'), 15);
INSERT INTO customer_authentication (customer_id, password, salt_id) VALUES (13, utl_raw.cast_to_raw('616f18e3f9202f3740544e01f3e76dd0da0a76af85e65563445afde0350c01c9'), 16);

-- #######################################
-- ##              TYPES                ##
-- #######################################

CREATE OR REPLACE TYPE product_type AS OBJECT (
    product_id   NUMBER(6, 0),
    product_name VARCHAR2(100),
    category     VARCHAR2(100)
);
/
CREATE OR REPLACE TYPE order_product_type AS OBJECT (
    product_id NUMBER(6, 0),
    quantity   NUMBER(6, 0)
);
/
CREATE OR REPLACE TYPE order_type AS OBJECT (
    customer_id NUMBER(6, 0),
    store_id    NUMBER(6, 0),
    product_id  NUMBER(6, 0),
    quantity    NUMBER(6, 0)
);
/
CREATE OR REPLACE TYPE review_type AS OBJECT (
    customer_id        NUMBER(6, 0),
    product_id         NUMBER(6, 0),
    review             NUMBER(1, 0),
    review_description VARCHAR2(1000)
);
/
CREATE OR REPLACE TYPE user_type AS OBJECT (
    id        NUMBER(6, 0),
    firstname VARCHAR2(50),
    lastname  VARCHAR2(50),
    address   VARCHAR2(100),
    email     VARCHAR2(100)
);
/
CREATE OR REPLACE TYPE warehouse_type AS OBJECT (
    warehouse_id    NUMBER(6, 0),
    warehouse_name  VARCHAR2(100)
);
/
CREATE OR REPLACE TYPE warehouse_product_type AS OBJECT (
    warehouse_id NUMBER(6, 0),
    product_id   NUMBER(6, 0),
    quantity     NUMBER(6, 0) 
);
/
CREATE OR REPLACE TYPE store_type AS OBJECT (
    store_id        NUMBER(6, 0),
    store_name      VARCHAR2(100)
);
/
CREATE OR REPLACE TYPE store_product_type AS OBJECT (
    store_id   NUMBER(6, 0),
    product_id NUMBER(6, 0),
    quantity   NUMBER(6, 0),
    price      NUMBER(8, 2)
);
/

-- #######################################
-- ##         CUSTOM EXCEPTIONS         ##
-- #######################################

CREATE OR REPLACE PACKAGE exceptions AS

    -- X not found exceptions

    product_not_found EXCEPTION;
    PRAGMA EXCEPTION_INIT(product_not_found, -20001);

    store_not_found EXCEPTION;
    PRAGMA EXCEPTION_INIT(store_not_found, -20002);

    warehouse_not_found EXCEPTION;
    PRAGMA EXCEPTION_INIT(warehouse_not_found, -20003);

    review_not_found EXCEPTION;
    PRAGMA EXCEPTION_INIT(review_not_found, -20004);

    -- X not in Y exceptions

    product_not_in_store EXCEPTION;
    PRAGMA EXCEPTION_INIT(product_not_in_store, -20101);

    product_not_in_warehouse EXCEPTION;
    PRAGMA EXCEPTION_INIT(product_not_in_warehouse, -20102);

    -- X already exists exceptions

    product_already_in_store EXCEPTION;
    PRAGMA EXCEPTION_INIT(product_already_in_store, -20111);

    product_already_in_warehouse EXCEPTION;
    PRAGMA EXCEPTION_INIT(product_already_in_warehouse, -20112);

    -- User input exceptions

    not_enough_stock EXCEPTION;
    PRAGMA EXCEPTION_INIT(not_enough_stock, -20201);

    invalid_review_bounds EXCEPTION;
    PRAGMA EXCEPTION_INIT(invalid_review_bounds, -20301);

    -- Authentication exceptions

    invalid_userpass_customer EXCEPTION;
    PRAGMA EXCEPTION_INIT(invalid_userpass_customer, -20501);

    invalid_userpass_employee EXCEPTION;
    PRAGMA EXCEPTION_INIT(invalid_userpass_employee, -20502);

END exceptions;

/

-- #######################################
-- ##           UTILS PACKAGE           ##
-- #######################################

CREATE OR REPLACE PACKAGE util AS

    FUNCTION store_exists (f_store_id IN NUMBER) RETURN NUMBER;
    FUNCTION product_exists (f_product_id IN NUMBER) RETURN NUMBER;
    FUNCTION warehouse_exists (f_warehouse_id IN NUMBER) RETURN NUMBER;
    FUNCTION review_exists (f_review_id IN NUMBER) RETURN NUMBER;

    FUNCTION store_has_product (f_store_id IN NUMBER, f_product_id IN NUMBER) RETURN NUMBER;
    FUNCTION warehouse_has_product (f_warehouse_id IN NUMBER, f_product_id IN NUMBER) RETURN NUMBER;

END util;
/
CREATE OR REPLACE PACKAGE BODY util AS

    /**
     * Checks if a store ID exists in the database
     * @param f_store_id The store ID to look for
     * @return Returns 1 (true) if the store exists, otherwise returns 0 (false)
     */
    FUNCTION store_exists (f_store_id IN NUMBER) RETURN NUMBER AS
        store_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO store_count FROM stores WHERE store_id = f_store_id;
        IF store_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END store_exists;

     /**
     * Checks if a warehouse ID exists in the database
     * @param f_warehouse_id The warehouse ID to look for
     * @return Returns 1 (true) if the warehouse exists, otherwise returns 0 (false)
     */
    FUNCTION warehouse_exists (f_warehouse_id IN NUMBER) RETURN NUMBER AS
        warehouse_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO warehouse_count FROM warehouses WHERE warehouse_id = f_warehouse_id;
        IF warehouse_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END warehouse_exists;

    /**
     * Checks if a review ID exists in the database
     * @param f_review_id The review id to look for
     * @returns` Returns 1 (true) if the review exists, otherwise returns 0 (false)
     */
    FUNCTION review_exists (f_review_id IN NUMBER) RETURN NUMBER AS
        review_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_count FROM reviews WHERE review_id = f_review_id;
        IF review_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END review_exists;

    /**
     * Checks if a product ID exists in the database
     * @param f_product_id product ID to look for
     * @return Returns 1 (true) if the product exists, otherwise returns 0 (false)
     */
    FUNCTION product_exists (f_product_id IN NUMBER) RETURN NUMBER AS
        product_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO product_count FROM products WHERE product_id = f_product_id;
        IF product_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END product_exists;

    /**
     * Checks if store has a specific product
     * @param f_product_id product ID to look for
     * @param f_store_id store ID to search product for
     * @return Returns 1 (true) if the product exists in that store, otherwise returns 0 (false)
     */
    FUNCTION store_has_product (f_store_id IN NUMBER, f_product_id IN NUMBER) RETURN NUMBER AS
        store_product_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO store_product_count
        FROM store_products
        WHERE store_id = f_store_id
        AND   product_id = f_product_id;

        IF store_product_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END store_has_product;

    /**
     * Checks if a warehouse has a specific product
     * @param f_product_id product ID to look for
     * @param f_warehouse_id warehouse ID to search product for
     * @return Returns 1 (true) if the product exists in that warehouse, otherwise returns 0 (false)
     */
    FUNCTION warehouse_has_product (f_warehouse_id IN NUMBER, f_product_id IN NUMBER) RETURN NUMBER AS
        warehouse_product_count NUMBER;
    BEGIN
        SELECT COUNT(*) INTO warehouse_product_count
        FROM warehouse_products
        WHERE warehouse_id = f_warehouse_id
        AND   product_id = f_product_id;

        IF warehouse_product_count > 0 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END warehouse_has_product;

END util;

/

-- #######################################
-- ##      CUSTOMER TOOLS PACKAGE       ##
-- #######################################

/**
 * Package used to interact with customer-related procedures and functions
 */
CREATE OR REPLACE PACKAGE store_customer_tools AS

    FUNCTION view_products RETURN SYS_REFCURSOR;
    FUNCTION view_stores RETURN SYS_REFCURSOR;
    FUNCTION view_products_for_store (f_store_id IN NUMBER) RETURN SYS_REFCURSOR;
    FUNCTION view_stores_for_product (f_product_id IN NUMBER) RETURN SYS_REFCURSOR;

    FUNCTION view_reviews (f_product_id IN NUMBER) RETURN SYS_REFCURSOR;
    FUNCTION view_reviews_by_customer (f_customer_id IN NUMBER) RETURN SYS_REFCURSOR;

    FUNCTION search_products_by_category (p_category IN VARCHAR2) RETURN SYS_REFCURSOR;
    FUNCTION search_product_across_stores (p_product_name IN VARCHAR2) RETURN SYS_REFCURSOR;

    FUNCTION view_orders (f_customer_id IN NUMBER) RETURN SYS_REFCURSOR;
    PROCEDURE place_order (p_order IN order_type);

    PROCEDURE add_review (p_review IN review_type);
    PROCEDURE flag_review (p_review_id IN NUMBER);

END store_customer_tools;
/
CREATE OR REPLACE PACKAGE BODY store_customer_tools AS
    /**
    * Retrieves information about all products in the database.
    * @return A cursor containing the product information.
    * @throws db_error Exception raised when there is an issue retrieving product information.
    */
    FUNCTION view_products RETURN SYS_REFCURSOR IS
        products_cursor SYS_REFCURSOR;
    BEGIN
        OPEN products_cursor FOR
            SELECT
                p.*,
                NVL(avg(r.review), 0) AS average_score,
                NVL(sum(sp.quantity), 0) AS total_quantity_across_stores
            FROM products p
            LEFT JOIN store_products sp ON p.product_id = sp.product_id
            LEFT JOIN reviews r ON p.product_id = r.product_id
            GROUP BY p.product_id, p.product_name, p.category
            ORDER BY p.product_id;

        RETURN products_cursor;
    END view_products;

    /**
    * Retrieves information about all stores in the database.
    * @return A cursor containing the store information.
    */
    FUNCTION view_stores RETURN SYS_REFCURSOR IS
        storess_cursor SYS_REFCURSOR;
    BEGIN
        OPEN storess_cursor FOR
            SELECT * FROM stores;
        RETURN storess_cursor;
    END view_stores;

    /**
     * Retrieves all products from a specific store.
     * @param f_store_id The id of the store to get products from.
     * @return Cursor containing product information (product_name VARCHAR2, category VARCHAR2, quantity NUMBER).
     * @throws store_not_found Exception raised when the specified store is not found.
     */
    FUNCTION view_products_for_store (f_store_id IN NUMBER) RETURN SYS_REFCURSOR AS
        v_cursor SYS_REFCURSOR;
    BEGIN
        IF util.store_exists(f_store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        OPEN v_cursor FOR
            SELECT p.product_id, p.product_name, p.category, sp.quantity, sp.price
            FROM products p
            LEFT JOIN store_products sp ON p.product_id = sp.product_id
            LEFT JOIN stores s ON sp.store_id = s.store_id
            WHERE s.store_id = f_store_id;

        RETURN v_cursor;
    END view_products_for_store;

    /**
     * Retrieves stores that have a given product.
     * @param f_product_id The name of the store to retrieve products from.
     * @return Cursor containing store information.
     * @throws product_not_found Exception raised when the specified product is not found.
     */
    FUNCTION view_stores_for_product (f_product_id IN NUMBER) RETURN SYS_REFCURSOR AS
        v_cursor SYS_REFCURSOR;
    BEGIN
        IF util.product_exists(f_product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        OPEN v_cursor FOR
            SELECT s.store_id, s.store_name, p.category, sp.quantity, sp.price
            FROM store_products sp
            LEFT JOIN stores s ON s.store_id = sp.store_id
            LEFT JOIN products p ON p.product_id = sp.product_id
            WHERE p.product_id = f_product_id;

        RETURN v_cursor;
    END view_stores_for_product;
    
    /**
     * Shows all reviews for a given product id
     * @param f_product_id The id of the product
     * @return Cursor containing all reviews from a specific product
     * @throws product_not_found Exception raised if product was not found
     */
    FUNCTION view_reviews (f_product_id IN NUMBER) RETURN SYS_REFCURSOR AS
        v_cursor SYS_REFCURSOR;
    BEGIN
        IF util.product_exists(f_product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        OPEN v_cursor FOR
            SELECT
                r.review_id,
                c.firstname || ' ' || c.lastname AS reviewer, 
                review,
                NVL(review_description, '(no written review)') AS review_description
            FROM reviews r
            LEFT JOIN customers c ON r.customer_id = c.customer_id
            WHERE product_id = f_product_id;

        RETURN v_cursor;
    END view_reviews;

    /**
     * Shows all reviews for a given customer id
     * @param f_customer_id The id of a customer
     * @returns Cursor containing all the reviews from a specific customer
     */
    FUNCTION view_reviews_by_customer(f_customer_id IN NUMBER) RETURN SYS_REFCURSOR AS
        reviews_cursor SYS_REFCURSOR;
    BEGIN
        OPEN reviews_cursor FOR
            SELECT
                r.review_id,
                p.product_name,
                r.review,
                NVL(r.review_description, '(no written review)') AS review_description
            FROM reviews r
            JOIN products p ON r.product_id = p.product_id
            WHERE r.customer_id = f_customer_id;
        RETURN reviews_cursor;
    END view_reviews_by_customer;

    /**
     * Searches the database for all products whose category matches the search
     * @param p_category The category to match
     * @return A cursor containing all of the matching products.
     */
    FUNCTION search_products_by_category(p_category IN VARCHAR2) RETURN SYS_REFCURSOR IS
        products_cursor SYS_REFCURSOR;
    BEGIN
        OPEN products_cursor FOR
            SELECT
                p.*,
                NVL(avg(r.review), 0) AS average_score,
                NVL(sum(sp.quantity), 0) AS total_quantity_across_stores
            FROM products p
            LEFT JOIN store_products sp ON p.product_id = sp.product_id
            LEFT JOIN reviews r ON p.product_id = r.product_id
            WHERE LOWER(p.category) LIKE '%' || LOWER(p_category) || '%'
            GROUP BY p.product_id, p.product_name, p.category
            ORDER BY p.product_id;

        RETURN products_cursor;
    END search_products_by_category;

    /**
     * Searches for a product across all stores.
     * @param p_product_name The name of the product to search for.
     * @return sys_refcursor A cursor containing the product information across stores.
     */
    FUNCTION search_product_across_stores (p_product_name IN VARCHAR2) RETURN SYS_REFCURSOR AS
        products_cursor SYS_REFCURSOR;
    BEGIN
        OPEN products_cursor FOR
            SELECT sp.store_id, sp.product_id, s.store_name, p.product_name, p.category, sp.quantity, sp.price
            FROM store_products sp
            LEFT JOIN products p ON sp.product_id = p.product_id
            LEFT JOIN stores s ON sp.store_id = s.store_id
            WHERE LOWER(p.product_name) LIKE '%' || LOWER(p_product_name) || '%'
            ORDER BY sp.product_id;

        RETURN products_cursor;
    END search_product_across_stores;

    /**
     * Searches for order for a given customer
     * @param f_customer_id The id of a customer
     * @return orders_cursor Cursor containing all the orders from a specific customer
     */
    FUNCTION view_orders (f_customer_id IN NUMBER) RETURN SYS_REFCURSOR AS
        orders_cursor SYS_REFCURSOR;
    BEGIN
        OPEN orders_cursor FOR
            SELECT order_id, store_name, product_name, quantity, price, order_date
            FROM orders
            LEFT JOIN stores USING (store_id)
            LEFT JOIN products USING (product_id)
            WHERE customer_id = f_customer_id;

        RETURN orders_cursor;
    END;

    /**
     * Allows an order to be placed
     * @param p_order The order information
     * @throws store_not_found if store does not exist
     * @throws product_not_found if product does not exist
     * @throws product_not_in_store if the product is not available in the store.
     * @throws not_enough_stock if the user attemps to order more than the store has available.
     */
    PROCEDURE place_order (p_order IN order_type) AS
        order_price        NUMBER(8, 2);
        available_quantity NUMBER(6, 0);
    BEGIN
        IF util.store_exists(p_order.store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        IF util.product_exists(p_order.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF util.store_has_product(p_order.store_id, p_order.product_id) = 0 THEN
            RAISE exceptions.product_not_in_store;
        END IF;

        SELECT price * p_order.quantity, quantity
        INTO order_price, available_quantity
        FROM store_products
        WHERE product_id = p_order.product_id
        AND   store_id = p_order.store_id;

        IF available_quantity < p_order.quantity THEN
            RAISE exceptions.not_enough_stock;
        END IF;

        INSERT INTO orders (customer_id, store_id, product_id, quantity, price, order_date)
        VALUES (p_order.customer_id, p_order.store_id, p_order.product_id, p_order.quantity, order_price, CURRENT_DATE);

        UPDATE store_products
        SET quantity = quantity - p_order.quantity
        WHERE product_id = p_order.product_id
        AND   store_id = p_order.store_id;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END place_order;

    /**
     * Adds a review on a product
     * @param p_review The review information
     * @throws product_not_found if the product is not in the database.
     * @throws invalid_review_bounds if the user gives a review under 1 or above 5.
     */
    PROCEDURE add_review (p_review IN review_type) AS
    BEGIN
        IF util.product_exists(p_review.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF p_review.review < 1 OR p_review.review > 5 THEN
            RAISE exceptions.invalid_review_bounds;
        END IF;

        INSERT INTO reviews (customer_id, product_id, review, review_description)
        VALUES (
            p_review.customer_id,
            p_review.product_id,
            p_review.review,
            p_review.review_description
        );

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_review;

    /**
     * Marks a review as sensitive for moderators to look at.
     * @param p_review_id The ID of the review to update.
     * @throws review_not_found If the specified review is not found.
     */
    PROCEDURE flag_review (p_review_id IN NUMBER) AS
        review_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_exists FROM reviews WHERE review_id = p_review_id;

        IF review_exists = 0 THEN
            RAISE exceptions.review_not_found;
        ELSE
            UPDATE reviews SET review_flag = review_flag + 1 WHERE review_id = p_review_id;
            COMMIT;
        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END flag_review;

END store_customer_tools;

/

-- #######################################
-- ##       MANAGER TOOLS PACKAGE       ##
-- #######################################

/**
 * Package used to interact with manager-related procedures and functions
 */
CREATE OR REPLACE PACKAGE store_manager_tools AS

    FUNCTION view_warehouses RETURN SYS_REFCURSOR;
    FUNCTION view_products_for_warehouse (f_warehouse_id IN NUMBER) RETURN SYS_REFCURSOR;
    FUNCTION search_product_across_warehouses(p_product_name IN VARCHAR2) RETURN SYS_REFCURSOR;
    FUNCTION view_audit_logs RETURN SYS_REFCURSOR;
    FUNCTION view_audit_logs_auth RETURN SYS_REFCURSOR;
    FUNCTION view_flagged_reviews RETURN SYS_REFCURSOR;
    FUNCTION show_most_popular_products RETURN SYS_REFCURSOR;

    PROCEDURE add_warehouse (p_warehouse IN warehouse_type);
    PROCEDURE update_warehouse (p_warehouse IN warehouse_type);
    PROCEDURE delete_warehouse (p_warehouse_id IN NUMBER, p_warehouse_name OUT VARCHAR2);

    PROCEDURE add_store (p_store IN store_type);
    PROCEDURE update_store (p_store IN store_type);
    PROCEDURE delete_store (p_store_id IN NUMBER, p_store_name OUT VARCHAR2);

    PROCEDURE add_product (p_product IN product_type);
    PROCEDURE update_product(p_product IN product_type);
    PROCEDURE delete_product (p_product_id IN NUMBER, p_product_name OUT VARCHAR2);

    PROCEDURE add_store_product (p_store_product IN store_product_type);
    PROCEDURE update_store_product (p_store_product IN store_product_type);

    PROCEDURE add_warehouse_product (p_warehouse_product IN warehouse_product_type);
    PROCEDURE update_warehouse_product (p_warehouse_product IN warehouse_product_type);

    PROCEDURE delete_review (p_review_id IN reviews.review_id%TYPE);

END store_manager_tools;
/
CREATE OR REPLACE PACKAGE BODY store_manager_tools AS

    /**
    * Retrieves information about all warehouses in the database.
    * @return A cursor containing the warehouse information.
    */
    FUNCTION view_warehouses RETURN SYS_REFCURSOR AS
        warehouses_cursor SYS_REFCURSOR;
    BEGIN
        OPEN warehouses_cursor FOR
            SELECT * FROM warehouses;
        RETURN warehouses_cursor;
    END view_warehouses;

    /**
     * Retrieves information about all products in a given warehouse.
     * @return A cursor containing the product information in the warehouse.
     * @throws warehouse_not_found if no warehouse was found
     */
    FUNCTION view_products_for_warehouse (f_warehouse_id IN NUMBER) RETURN SYS_REFCURSOR AS
        v_cursor SYS_REFCURSOR;
    BEGIN
        IF util.warehouse_exists(f_warehouse_id) = 0 THEN
            RAISE exceptions.warehouse_not_found;
        END IF;

        OPEN v_cursor FOR
            SELECT p.product_id, p.product_name, p.category, wp.quantity
            FROM products p
            LEFT JOIN warehouse_products wp ON p.product_id = wp.product_id
            LEFT JOIN warehouses w ON wp.warehouse_id = w.warehouse_id
            WHERE w.warehouse_id = f_warehouse_id;

        RETURN v_cursor;
    END view_products_for_warehouse;

    /**
     * Searches for a product across all warehouses.
     * @param p_product_name The name of the product to search for.
     * @return A cursor containing the product information across warehouses.
     */
    FUNCTION search_product_across_warehouses (p_product_name IN VARCHAR2) RETURN SYS_REFCURSOR AS
        products_cursor SYS_REFCURSOR;
    BEGIN
        OPEN products_cursor FOR
            SELECT wp.warehouse_id, wp.product_id, w.warehouse_name, p.product_name, p.category, wp.quantity
            FROM warehouse_products wp
            LEFT JOIN products p ON wp.product_id = p.product_id
            LEFT JOIN warehouses w ON wp.warehouse_id = w.warehouse_id
            WHERE LOWER(p.product_name) LIKE '%' || LOWER(p_product_name) || '%'
            ORDER BY wp.product_id;

        RETURN products_cursor;
    END search_product_across_warehouses;

    /**
     * Retrieves the audit logs containing information about changes to important tables.
     * @return A cursor containing the audit log information.
     */
    FUNCTION view_audit_logs RETURN SYS_REFCURSOR AS
        audit_logs_cursor SYS_REFCURSOR;
    BEGIN
        OPEN audit_logs_cursor FOR
            SELECT * FROM audit_log;
        RETURN audit_logs_cursor;
    END view_audit_logs;

    /**
     * Retrieves the logs of when the authentication functions are invoked.
     * @return A cursor containing the auth audit log atuh information.
     */
    FUNCTION view_audit_logs_auth RETURN SYS_REFCURSOR AS
        audit_logs_auth_cursor SYS_REFCURSOR;
    BEGIN
        OPEN audit_logs_auth_cursor FOR
            SELECT * FROM audit_log_auth;
        RETURN audit_logs_auth_cursor;
    END view_audit_logs_auth;

    /**
     * Searches for reviews that have been flagged more than once by users
     * @return Cursor containing all flagged reviews
     */
    FUNCTION view_flagged_reviews RETURN SYS_REFCURSOR AS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT
                review_id,
                review_flag,
                firstname || ' ' || lastname AS reviewer,
                product_name,
                review,
                NVL(review_description, '(no written review)') AS review_description
            FROM reviews
            LEFT JOIN customers USING (customer_id)
            LEFT JOIN products USING (product_id)
            WHERE review_flag > 1;

        RETURN v_cursor;

    END view_flagged_reviews;

    /**
     * Retrieves information about the most popular products based on the number of times they have been ordered.
     * @return A cursor containing product information along with order count.
     */
    FUNCTION show_most_popular_products RETURN SYS_REFCURSOR AS
        popular_products_cursor SYS_REFCURSOR;
    BEGIN
        OPEN popular_products_cursor FOR
            SELECT
                p.*,
                COUNT(*) AS order_count,
                NVL(AVG(r.review), 0) AS average_score
            FROM  products p
            LEFT JOIN orders o ON p.product_id = o.product_id
            LEFT JOIN reviews r ON r.product_id = o.product_id
            GROUP BY p.product_id, p.product_name, p.category
            HAVING COUNT(o.product_id) > 0
            ORDER BY order_count DESC;

        RETURN popular_products_cursor;
    END show_most_popular_products;

    /**
     * Adds a new warehouse to the database.
     * @param p_warehouse The warehouse information to add.
     */
    PROCEDURE add_warehouse(p_warehouse IN warehouse_type) AS
    BEGIN
        INSERT INTO warehouses (warehouse_name) VALUES (p_warehouse.warehouse_name);
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_warehouse;

     /**
     * Updates an existing warehouse's information.
     * @param p_warehouse The warehouse information to update.
     * @throws warehouse_not_found if no warehouse was found
     */
    PROCEDURE update_warehouse(p_warehouse IN warehouse_type) AS
    BEGIN
        IF util.warehouse_exists(p_warehouse.warehouse_id) = 0 THEN
            RAISE exceptions.warehouse_not_found;
        END IF;

        UPDATE warehouses
        SET warehouse_name = p_warehouse.warehouse_name
        WHERE warehouse_id = p_warehouse.warehouse_id;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END update_warehouse;

    /**
     * Deletes a warehouse from the database.
     * @param p_warehouse_id The ID of the warehouse to delete.
     * @throws warehouse_not_found if no warehouse was found
     */
    PROCEDURE delete_warehouse (p_warehouse_id IN NUMBER, p_warehouse_name OUT VARCHAR2) AS
    BEGIN
        IF util.warehouse_exists(p_warehouse_id) = 0 THEN
            RAISE exceptions.warehouse_not_found;
        END IF;

        SELECT warehouse_name INTO p_warehouse_name
        FROM warehouses
        WHERE warehouse_id = p_warehouse_id;

        DELETE FROM warehouses WHERE warehouse_id = p_warehouse_id;
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;

    END delete_warehouse;

    /**
     * Adds a new store to the database.
     * @param p_store Store information to be added.
     */
    PROCEDURE add_store(p_store IN store_type) AS
    BEGIN
        INSERT INTO stores (store_name) VALUES (p_store.store_name);
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_store;

    /**
     * Updates an existing store in the database.
     * @param p_store Store information to be updated.
     * @throws store_not_found if no store was found
     */
    PROCEDURE update_store(p_store IN store_type) AS
    BEGIN
        IF util.store_exists(p_store.store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        UPDATE stores
        SET store_name = p_store.store_name
        WHERE store_id = p_store.store_id;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END update_store;

    /**
     * Deletes a store from the database.
     * @param p_store_id ID of the store to be deleted.
     * @throws store_not_found if no store was found
     */
    PROCEDURE delete_store(p_store_id IN NUMBER, p_store_name OUT VARCHAR2) AS
    BEGIN
        IF util.store_exists(p_store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        SELECT store_name INTO p_store_name
        FROM stores
        WHERE store_id = p_store_id;

        DELETE FROM stores WHERE store_id = p_store_id;
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;

    END delete_store;

    /**
     * Adds a product to the database
     * @param p_product The product to be created
     * @throws product_not_found if no product was found
     */
    PROCEDURE add_product (p_product IN product_type) AS
    BEGIN
        INSERT INTO products (product_name, category) VALUES (p_product.product_name, p_product.category);
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_product;

    /**
     * Updates a product in the database
     * @param p_product The product id to be updated
     * @throws product_not_found if no product was found
     */
    PROCEDURE update_product(p_product IN product_type) AS
    BEGIN
        IF util.product_exists(p_product.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;
        
        -- If the user enters - it's because they requested not to change the given field.
        IF p_product.product_name != '-' THEN
            UPDATE products
            SET product_name = p_product.product_name
            WHERE product_id = p_product.product_id;
        END IF;

        -- If the user enters - it's because they requested not to change the given field.
        IF p_product.category != '-' THEN
            UPDATE products
            SET category = p_product.category
            WHERE product_id = p_product.product_id;
        END IF;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END update_product;

    /**
     * Deletes a product to the database
     * @param p_product The product to be deleted
     * @throws product_not_found if no product was found
     */
    PROCEDURE delete_product(p_product_id IN NUMBER, p_product_name OUT VARCHAR2) AS
    BEGIN
        IF util.product_exists(p_product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        SELECT product_name INTO p_product_name
        FROM products
        WHERE product_id = p_product_id;

        DELETE FROM products WHERE product_id = p_product_id;
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;

    END delete_product;

    /**
     * Adds a store_product to the database
     * @param store_product_type The product to be created
     * @throws product_not_found if no product was found
     * @throws store_not_found if no store was found
     * @throws product_already_in_store if the product is already in the store
     */
    PROCEDURE add_store_product (p_store_product IN store_product_type) AS
    BEGIN
        IF util.store_exists(p_store_product.store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        IF util.product_exists(p_store_product.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF util.store_has_product(p_store_product.store_id, p_store_product.product_id) = 1 THEN
            RAISE exceptions.product_already_in_store;
        END IF;

        INSERT INTO store_products (store_id, product_id, quantity, price) VALUES (
            p_store_product.store_id,
            p_store_product.product_id,
            p_store_product.quantity,
            p_store_product.price
        );

        COMMIT;

    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_store_product;

    /**
     * Updates the information of a product in a store
     * @param p_store_product The store product information
     * @throws store_not_found if no store was found
     * @throws product_not_found if no product was found
     * @throws product_not_in_store if that product isn't available in that store
     */
    PROCEDURE update_store_product (p_store_product IN store_product_type) AS
    BEGIN
        IF util.store_exists(p_store_product.store_id) = 0 THEN
            RAISE exceptions.store_not_found;
        END IF;

        IF util.product_exists(p_store_product.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF util.store_has_product(p_store_product.store_id, p_store_product.product_id) = 0 THEN
            RAISE exceptions.product_not_in_store;
        END IF;

        -- if the user enters -1 its because they requested not to change that field
        IF p_store_product.quantity != -1 THEN
            UPDATE store_products
            SET quantity = p_store_product.quantity
            WHERE store_id = p_store_product.store_id
            AND   product_id = p_store_product.product_id;
        END IF;

        -- if the user enters -1 its because they requested not to change that field
        IF p_store_product.price != -1 THEN
            UPDATE store_products
            SET price = p_store_product.price
            WHERE store_id = p_store_product.store_id
            AND   product_id = p_store_product.product_id;
        END IF;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END update_store_product;

    /**
     * Adds a warehouse_product to the database
     * @param p_product The product to be created
     * @throws product_not_found if no product was found
     */
    PROCEDURE add_warehouse_product (p_warehouse_product IN warehouse_product_type) AS
    BEGIN
        IF util.warehouse_exists(p_warehouse_product.warehouse_id) = 0 THEN
            RAISE exceptions.warehouse_not_found;
        END IF;

        IF util.product_exists(p_warehouse_product.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF util.warehouse_has_product(p_warehouse_product.warehouse_id, p_warehouse_product.product_id) = 1 THEN
            RAISE exceptions.product_already_in_warehouse;
        END IF;

        INSERT INTO warehouse_products (warehouse_id, product_id, quantity) VALUES (
            p_warehouse_product.warehouse_id,
            p_warehouse_product.product_id,
            p_warehouse_product.quantity
        );

        COMMIT;

    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END add_warehouse_product;

    /**
     * Updates the quantity of a product for a given warehouse
     * @param p_warehouse_product The warehouse product information
     * @throws warehouse_not_found if no warehouse was found
     * @throws product_not_found if no product was found
     * @throws product_not_in_warehouse if that product doesn't exist in that warehouse
     */
    PROCEDURE update_warehouse_product (p_warehouse_product IN warehouse_product_type) AS
    BEGIN
        IF util.warehouse_exists(p_warehouse_product.warehouse_id) = 0 THEN
            RAISE exceptions.warehouse_not_found;
        END IF;

        IF util.product_exists(p_warehouse_product.product_id) = 0 THEN
            RAISE exceptions.product_not_found;
        END IF;

        IF util.warehouse_has_product(p_warehouse_product.warehouse_id, p_warehouse_product.product_id) = 0 THEN
            RAISE exceptions.product_not_in_warehouse;
        END IF;

        UPDATE warehouse_products
        SET quantity = p_warehouse_product.quantity
        WHERE warehouse_id = p_warehouse_product.warehouse_id
        AND   product_id = p_warehouse_product.product_id;

        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END update_warehouse_product;

   /**
    * Deletes a review for a given review ID
    * @param p_review_id The ID of the review to be deleted
    * @throws review_not_found if review Id does not exist
    */
    PROCEDURE delete_review (p_review_id IN reviews.review_id%TYPE) AS
    BEGIN
        IF util.review_exists(p_review_id) = 0 THEN
            RAISE exceptions.review_not_found;
        END IF;

        DELETE FROM reviews WHERE review_id = p_review_id;
        COMMIT;
    EXCEPTION
        WHEN others THEN
            ROLLBACK;
            RAISE;
    END delete_review;

END store_manager_tools;

/

-- #######################################
-- ##       AUTHENTICATION PACKAGE      ##
-- #######################################

CREATE OR REPLACE PACKAGE authentication AS

    FUNCTION get_customer_salt (f_customer_email IN customers.email%TYPE) RETURN RAW;
    FUNCTION get_employee_salt (f_employee_email IN employees.email%TYPE) RETURN RAW;
    FUNCTION validate_customer_authentication (f_customer_email IN customers.email%TYPE, f_password_hash IN RAW) RETURN user_type;
    FUNCTION validate_employee_authentication (f_employee_email IN employees.email%TYPE, f_password_hash IN RAW) RETURN user_type;
    
END authentication;
/
CREATE OR REPLACE PACKAGE BODY authentication AS

    /**
     * Gets the authentication salt for a given customer's email.
     * @param f_customer_email The customer's email.
     * @return The salt in RAW format.
     */
    FUNCTION get_customer_salt (f_customer_email IN customers.email%TYPE) RETURN RAW AS 
        customer_salt authentication_salt.salt%TYPE;
    BEGIN
        INSERT INTO audit_log_auth (user_type, input, action)
        VALUES ('customer', f_customer_email, 'get_customer_salt');

        COMMIT;

        SELECT salt
        INTO customer_salt
        FROM authentication_salt
        LEFT JOIN customer_authentication USING (salt_id)
        LEFT JOIN customers USING (customer_id)
        WHERE email = f_customer_email;

        RETURN customer_salt;

    EXCEPTION
        WHEN no_data_found THEN
            dbms_output.put_line('Error in get_customer_salt: No data found');
            RAISE exceptions.invalid_userpass_customer;
    END get_customer_salt;

    /**
     * Gets the authentication salt for a given employee's email.
     * @param f_employee_email The employee's email.
     * @return The salt in RAW format.
     */
    FUNCTION get_employee_salt (f_employee_email IN employees.email%TYPE) RETURN RAW AS 
        employee_salt authentication_salt.salt%TYPE;
    BEGIN
        INSERT INTO audit_log_auth (user_type, input, action)
        VALUES ('employee', f_employee_email, 'get_employee_salt');

        COMMIT;

        SELECT salt
        INTO employee_salt
        FROM authentication_salt
        LEFT JOIN employee_authentication USING (salt_id)
        LEFT JOIN employees USING (employee_id)
        WHERE email = f_employee_email;

        RETURN employee_salt;

    EXCEPTION
        WHEN no_data_found THEN
            dbms_output.put_line('Error in get_employee_salt: No data found');
            RAISE exceptions.invalid_userpass_employee;
    END get_employee_salt;

    /**
     * Attempts to authenticate a customer using an email and hashed, salted password.
     * @param f_customer_email The customer's email.
     * @param f_password_hash The hashed and salted password that will be used in the attempted login.
     * @return The customer's information if the login was successful.
     * @throws exceptions.invalid_userpass_customer If the email is not present in the customer database OR the password hashes don't match.
     */
    FUNCTION validate_customer_authentication (f_customer_email IN customers.email%TYPE, f_password_hash IN RAW) RETURN user_type AS
        db_customer_firstname     VARCHAR2(50);
        db_customer_lastname      VARCHAR2(50);
        db_customer_address       VARCHAR2(100);
        db_customer_email         VARCHAR2(100);

        db_customer_id            customers.customer_id%TYPE;
        db_customer_password_hash customer_authentication.password%TYPE;
    BEGIN
        SELECT customer_id
        INTO db_customer_id
        FROM customers
        WHERE email = f_customer_email;

        IF db_customer_id IS NULL THEN
            INSERT INTO audit_log_auth (user_type, input, action, description)
            VALUES ('customer', f_customer_email, 'validate_customer_authentication', 'fail (customer not found)');

            RAISE exceptions.invalid_userpass_customer;
        END IF;

        SELECT password
        INTO db_customer_password_hash
        FROM customer_authentication
        WHERE customer_id = db_customer_id;

        IF db_customer_password_hash != f_password_hash THEN
            INSERT INTO audit_log_auth (user_type, input, action, description)
            VALUES ('customer', f_customer_email, 'validate_customer_authentication', 'fail (wrong password)');

            RAISE exceptions.invalid_userpass_customer;
        END IF;

        SELECT 
            firstname, lastname, address , email    
            INTO db_customer_firstname, db_customer_lastname, db_customer_address, db_customer_email
        FROM customers
        WHERE customer_id = db_customer_id;

        INSERT INTO audit_log_auth (user_type, input, action, description)
        VALUES ('customer', f_customer_email, 'validate_customer_authentication', 'success');

        RETURN user_type (
            db_customer_id,
            db_customer_firstname,
            db_customer_lastname,
            db_customer_address,
            db_customer_email
        );
    EXCEPTION
        WHEN exceptions.invalid_userpass_customer THEN
            DBMS_OUTPUT.PUT_LINE('Error in validate_customer_authentication: Wrong username or password entered.');
            RAISE;

    END validate_customer_authentication;

    /**
     * Attempts to authenticate a employee using an email and hashed, salted password.
     * @param f_employee_email The employee's email.
     * @param f_password_hash The hashed and salted password that will be used in the attempted login.
     * @return The employee's information if the login was successful.
     * @throws exceptions.invalid_userpass_employee If the email is not present in the employee database OR the password hashes don't match.
     */
    FUNCTION validate_employee_authentication (f_employee_email IN employees.email%TYPE, f_password_hash IN RAW) RETURN user_type AS
        db_employee_firstname     VARCHAR2(50);
        db_employee_lastname      VARCHAR2(50);
        db_employee_address       VARCHAR2(100);
        db_employee_email         VARCHAR2(100);

        db_employee_id            employees.employee_id%TYPE;
        db_employee_password_hash employee_authentication.password%TYPE;
    BEGIN
        SELECT employee_id
        INTO db_employee_id
        FROM employees
        WHERE email = f_employee_email;

        IF db_employee_id IS NULL THEN
            INSERT INTO audit_log_auth (user_type, input, action, description)
            VALUES ('employee', f_employee_email, 'validate_employee_authentication', 'fail (employee not found)');

            RAISE exceptions.invalid_userpass_employee;
        END IF;

        SELECT password
        INTO db_employee_password_hash
        FROM employee_authentication
        WHERE employee_id = db_employee_id;

        IF db_employee_password_hash != f_password_hash THEN
            INSERT INTO audit_log_auth (user_type, input, action, description)
            VALUES ('employee', f_employee_email, 'validate_employee_authentication', 'fail (wrong password)');

            RAISE exceptions.invalid_userpass_employee;
        END IF;

        SELECT 
            firstname, lastname, address , email    
            INTO db_employee_firstname, db_employee_lastname, db_employee_address, db_employee_email
        FROM employees
        WHERE employee_id = db_employee_id;

        INSERT INTO audit_log_auth (user_type, input, action, description)
        VALUES ('employee', f_employee_email, 'validate_employee_authentication', 'success');

        RETURN user_type (
            db_employee_id,
            db_employee_firstname,
            db_employee_lastname,
            db_employee_address,
            db_employee_email
        );
    EXCEPTION
        WHEN exceptions.invalid_userpass_employee THEN
            DBMS_OUTPUT.PUT_LINE('Error in validate_employee_authentication: Wrong username or password entered.');
            RAISE;

    END validate_employee_authentication;

END authentication;

/

-- #######################################
-- ##             AUDIT LOGS            ##
-- #######################################
/**
 * This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'products' table
 * by capturing relevant information and storing it in the 'audit_log' table.
 */
CREATE OR REPLACE TRIGGER product_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON products
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Product <name, category> = <' || :new.product_name || ', ' || :new.category || '>';
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Name <old, new> = <' || :old.product_name || ', ' || :new.product_name || '> | ' || 
                       'Category <old, new> = <' || :old.category || ', ' || :new.category || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Product <name, category> = <' || :old.product_name || ', ' || :old.category || '>';
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('products', action, information);

END product_audit_trigger;

/

/**
* This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'stores' table
* by capturing relevant information and storing it in the 'audit_log' table.
*/
CREATE OR REPLACE TRIGGER store_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON stores
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Store <name> = <' || :new.store_name || '>';
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Name <old, new> = <' || :old.store_name || ', ' || :new.store_name || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Store <name> = <' || :old.store_name || '>';
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('stores', action, information);

END store_audit_trigger;

/

/**
* This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'warehouses' table
* by capturing relevant information and storing it in the 'audit_log' table.
*/
CREATE OR REPLACE TRIGGER warehouse_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON warehouses
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Warehouse <name> = <' || :new.warehouse_name || '>';
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Name <old, new> = <' || :old.warehouse_name || ', ' || :new.warehouse_name || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Warehouse <name> = <' || :old.warehouse_name || '>';
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('warehouses', action, information);

END warehouse_audit_trigger;

/

/**
* This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'store_products' table
* by capturing relevant information and storing it in the 'audit_log' table.
*/
CREATE OR REPLACE TRIGGER store_product_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON store_products
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Store Product <store, product, quantity> = <' || :new.store_id || ', ' || :new.product_id || ', ' || :new.quantity || '>';
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Quantity <old, new> = <' || :old.quantity || ', ' || :new.quantity || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Store Product <store, product> = <' || :old.store_id || ', ' || :old.product_id || '>';
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('store_products', action, information);

END store_product_audit_trigger;

/

/**
* This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'warehouse_products' table
* by capturing relevant information and storing it in the 'audit_log' table.
*/
CREATE OR REPLACE TRIGGER warehouse_product_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON warehouse_products
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Warehouse Product <warehouse, product, quantity> = <' || :new.warehouse_id || ', ' || :new.product_id || ', ' || :new.quantity || '>';
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Quantity <old, new> = <' || :old.quantity || ', ' || :new.quantity || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Warehouse Product <warehouse, product> = <' || :old.warehouse_id || ', ' || :old.product_id || '>';
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('warehouse_products', action, information);

END warehouse_product_audit_trigger;

/

/**
* This trigger is designed to audit changes (INSERT, UPDATE, DELETE) on the 'reviews' table
* by capturing relevant information and storing it in the 'audit_log' table.
*/
CREATE OR REPLACE TRIGGER review_audit_trigger
AFTER INSERT OR UPDATE OR DELETE
ON reviews
FOR EACH ROW
DECLARE
    action      VARCHAR2(6);
    information VARCHAR2(512);
BEGIN

    IF INSERTING THEN
        action := 'INSERT';
        information := 'Review Added ' || :new.review_id;
    ELSIF UPDATING THEN
        action := 'UPDATE';
        information := 'Flag <old, new> = <' || :old.review_flag || ', ' || :new.review_flag || '>';
    ELSIF DELETING THEN
        action := 'DELETE';
        information := 'Review Deleted ' || :old.review_id;
    END IF;

    INSERT INTO audit_log (table_name, action, information)
    VALUES ('reviews', action, information);

END review_audit_trigger;
