# Superstore
## Database project
By `Sriraam Nadarajah` and `Andrew Rokakis`

### Instructions
1. Run `setup.sql` to get the entire superstore setup.
2. Make sure the Java application is running in a `unix-based` terminal (eg. git bash) and NOT the `default windows` terminal.
3. Run `remove.sql` to remove everything created by the setup script.

### Using the program
1. When you open the program, you will be prompted to enter your PDBORA19c credentials.
2. Once logged into the database, you will be asked if you want to log in as a customer or as an employee.
3. You must enter the email and password of the user type (customer/employee) you selected.

### User login credentials
* The emails are the ones present in the `setup.sql` script. The passwords to the accounts are the emails with the domain portion stripped **(eg. alex@gmail.com -> alex)**.
* The database only stores hashed and salted passwords.
* Here are some logins you can use to test;

    | User type | Email | Password |
    | - | - | - |
    | Customer | b.a@gmail.com | b.a |
    | Customer | alex@gmail.com | alex |
    | Employee | a@a.com | a |
    | Employee | potus@whitehouse.gov | potus |

### Running commands
* Not all commands are visible on the home page, some of them are only shown when you enter a relevant command page **(eg. the command to add a review will be visible only on pages where products are shown)**.
* Full list of commands and technical details can be found in the `superstore.cmds` package of the Java application.
* There are 3 types of commands;
    - **Shared** commands can be run by both customers and employees.
    - **Customer** commands can only be run by a customer.
    - **Employee** commands can only be run by an employee.
* These commands can be run from anywhere;
    - `b` - Exits the current command and goes to the home screen.
    - `q` - Exits the program.